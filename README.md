Configuration Log Repository Manager
==================================================


Features Summary
----------------

- With the V2 Configuration Log, System Admins can easily track all changes & modifications made in their Salesforce org. Filter by component, date & time and user to create specific configuration reports that can be saved and downloaded.
- Create project specific configuration logs or search and access a detailed log of all changes made within a Salesforce org.
- The tool has an in platform user guide


Usage Information and Known Issues
----------------------------------

- **Platform SOQL Limits** This tool uses DML and SOQL queries, which makes it subject to platform limitations
- **Deployment** This tool dynamically deploys a test factory, test classes, apex classes, apex triggers, custom objects, custom fields, validation rules, a process builder, a custom notification, and a static resource
- **Sandbox Testing** While the tool can be installed and enabled directly in production, sandbox testing is still strongly recommended.
- **Requirements** This tool requires account team and opportunity team for their deployment.

Documentation
----------------------------------
More details about the tool can be found [here](https://v2analyst.atlassian.net/wiki/spaces/ARCHDEV/pages/1171259412/Configuration+Log+Project)


Packaged Release History
------------------------

**Latest Release Version 1.10**
_______________________________

[Package URL](https://login.salesforce.com/packaging/installPackage.apexp?p0=04t4x000000Mq4w)

Installing the Source Code (Developers)
=======================================

This project uses Salesforce DX for development and packaging. If you want to deploy the unmanaged version of this to your sandbox or production org you can use the Salesforce DX convert and deploy commands to do so. However the recommended deployment for these orgs is via the managed package links above.
