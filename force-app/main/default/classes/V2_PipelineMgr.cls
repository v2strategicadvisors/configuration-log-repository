public class V2_PipelineMgr {
    
    public class customTypesException extends Exception {}
    private static Set<string> lookupTypeList;
    private static Set<string> activePlatfromList;
    private static Set<string> requireMultiSelect;
    
   	public class LocalSelect {
     	@AuraEnabled
        public string value {get; set;}
        @AuraEnabled
        public string label {get; set;}
        
        public LocalSelect(string value, string label){           
            this.value = value;
            this.label = label;
        }
    }
    
    public class FieldsMetadata {
        @AuraEnabled public List<V2SACL1__Prod_Sel_Core_Cols__mdt> coreColumns;
        @AuraEnabled public List<V2SACL1__Platform_Entry_Type__mdt> platformEntryTypes;
                
        public FieldsMetadata(){
            this.coreColumns = new List<V2SACL1__Prod_Sel_Core_Cols__mdt>();
            this.platformEntryTypes = new List<V2SACL1__Platform_Entry_Type__mdt>();
        }
    }
    
    public class PipelineDetailsByPlatform {
        @AuraEnabled public String platform;
        @AuraEnabled public List<V2SACL1__Pipeline_Detail__c> pipelineDetails;
                
        public PipelineDetailsByPlatform(String platform){
            this.platform = platform;
            this.pipelineDetails = new List<V2SACL1__Pipeline_Detail__c>();
        }
    }
       
    public class PipelineProductsByPlatform {
        @AuraEnabled public String platform;
        @AuraEnabled public List<V2SACL1__Pipeline_Product__c> products;
        @AuraEnabled public Boolean isProgram;
        @AuraEnabled public Boolean requiredMultiSelect;
                
        public PipelineProductsByPlatform(String platform){
            this.platform = platform;
            this.products = new List<V2SACL1__Pipeline_Product__c>();
            this.isProgram = lookupTypeList.contains(platform);
            this.requiredMultiSelect = requireMultiSelect.contains(platform);
        }
    }    
    
    public class Metadata {
        @AuraEnabled public FieldsMetadata fields;
        @AuraEnabled public Map<String, PipelineProductsByPlatform> prodsByPlatform;
    }
    
    private static String constructPipelineDetailsQuery(){
        FieldsMetadata fieldsMeta = getFieldsMetadata();
		
    	Set<String> pipelineDetailsFieldsToQuery = new Set<String>();

        //Go through the records of the prod core cols metadata type
        //and save the api name values in the pipelineDetailsFieldsToQuery set
        for(V2SACL1__Prod_Sel_Core_Cols__mdt coreCol : fieldsMeta.coreColumns){
			pipelineDetailsFieldsToQuery.add(coreCol.V2SACL1__ApiName__c);       
        }
        
        String pipelineDetailQuery = 'SELECT V2SACL1__Pipeline_Product__r.V2SACL1__Family__c, V2SACL1__Pipeline_Product__r.V2SACL1__Month_Issue_Close_Date__c, V2SACL1__BrandVoice_Program__c, V2SACL1__Sub_Category__c, ';
        
        for(String sfield : pipelineDetailsFieldsToQuery) {
            pipelineDetailQuery += sfield + ',';
        }  
        pipelineDetailQuery = pipelineDetailQuery.removeEnd(',');
        
        pipelineDetailQuery += ' FROM V2SACL1__Pipeline_Detail__c WHERE V2SACL1__Opportunity__c =: opportunityId and '+
           '(V2SACL1__Pipeline_Product__r.V2SACL1__Month_Issue_Close_Date__c = LAST_YEAR or ' +
           'V2SACL1__Pipeline_Product__r.V2SACL1__Month_Issue_Close_Date__c = THIS_YEAR or ' +
           'V2SACL1__Pipeline_Product__r.V2SACL1__Month_Issue_Close_Date__c = NEXT_YEAR) ORDER BY V2SACL1__Pipeline_Product__r.V2SACL1__Month_Issue_Close_Date__c';

        system.debug('LR:' + pipelineDetailQuery);

        return pipelineDetailQuery;
    }
    
    /**
    * @description This method returns the existing pipelineDetails grouped by platform
    * @param opportunityId refers to Id of opportunity being worked with
    */
    @AuraEnabled
    public static List<PipelineDetailsByPlatform> getExistingPipelineDetailsByPlatform(Id opportunityId){
        List<PipelineDetailsByPlatform> platformPipelineDetails = new List<PipelineDetailsByPlatform>();
        
        List<V2SACL1__Pipeline_Detail__c> listPipelineDetails = new List<V2SACL1__Pipeline_Detail__c>();
		
		String pipelineDetailQuery = constructPipelineDetailsQuery();        
        try {
            listPipelineDetails = Database.query(pipelineDetailQuery);
        } catch(QueryException e) {
            System.debug('=== QueryException ---> ' + e);
            throw new customTypesException('Contact your administrator, something is wrong with the custom metadata types configuration');
        }
                       
        String currProdFam = '';
        PipelineDetailsByPlatform platPipelineDetail = new PipelineDetailsByPlatform(currProdFam);
        for(V2SACL1__Pipeline_Detail__c pipelineDetail : listPipelineDetails){             
            if(pipelineDetail.V2SACL1__Pipeline_Product__r.V2SACL1__Family__c != currProdFam){
                currProdFam = pipelineDetail.V2SACL1__Pipeline_Product__r.V2SACL1__Family__c;
                platPipelineDetail = new PipelineDetailsByPlatform(currProdFam);                
                platformPipelineDetails.add(platPipelineDetail);
            }
            
            platPipelineDetail.pipelineDetails.add(pipelineDetail);
        }
        system.debug('pipelineDetails: ' + platformPipelineDetails);
        return platformPipelineDetails;  
    }
    
    private static FieldsMetadata getFieldsMetadata(){
		List<V2SACL1__Platform_Entry_Type__mdt> platEntryTypes;

        platEntryTypes = [SELECT Id, V2SACL1__Platform_Name__c, V2SACL1__Entry_Type__c, V2SACL1__Is_Active__c, V2SACL1__Multi_Select_Picklit__c FROM V2SACL1__Platform_Entry_Type__mdt WHERE V2SACL1__Is_Active__c = true];
   
        lookupTypeList = new Set<String>();
        activePlatfromList = new Set<String>();
        requireMultiSelect = new Set<String>();

        for (V2SACL1__Platform_Entry_Type__mdt pe : platEntryTypes) {
            activePlatfromList.add(pe.V2SACL1__Platform_Name__c);

            if (pe.V2SACL1__Entry_Type__c == 'Lookup') {
                lookupTypeList.add(pe.V2SACL1__Platform_Name__c);
            }
            if (pe.V2SACL1__Multi_Select_Picklit__c) {
                requireMultiSelect.add(pe.V2SACL1__Platform_Name__c);
            }
        }

        FieldsMetadata fieldsMeta = new FieldsMetadata();
        fieldsMeta.platformEntryTypes = platEntryTypes;
        
        List<V2SACL1__Prod_Sel_Core_Cols__mdt> coreColumns = [SELECT MasterLabel, V2SACL1__ApiName__c, V2SACL1__Is_Active__c, V2SACL1__Type__c,
                                                     V2SACL1__Order__c, V2SACL1__ColumnWidth__c FROM V2SACL1__Prod_Sel_Core_Cols__mdt WHERE V2SACL1__Is_Active__c = true ORDER BY V2SACL1__Order__c ASC];
        
        fieldsMeta.coreColumns = coreColumns;
        
        return fieldsMeta;
    }


    private static Map<String, PipelineProductsByPlatform> getProductsListByPlatform(Id opportunityId){
        
        List<V2SACL1__Pipeline_Product__c> products = [SELECT Id, Name, V2SACL1__Month_Issue_Close_Date__c, V2SACL1__Family__c FROM V2SACL1__Pipeline_Product__c WHERE V2SACL1__Family__c IN: activePlatfromList AND V2SACL1__Is_Active__c = true 
                                   AND (V2SACL1__Month_Issue_Close_Date__c = LAST_YEAR or 
                                   V2SACL1__Month_Issue_Close_Date__c = THIS_YEAR or
                                   V2SACL1__Month_Issue_Close_Date__c = NEXT_YEAR) ORDER BY V2SACL1__Month_Issue_Close_Date__c ASC];

        System.debug('**** products ---> ' + products);
        
        
        /*===================================================================================*/
        Map<String, PipelineProductsByPlatform> mapPlatforms = new Map<String, PipelineProductsByPlatform>();
        for(V2SACL1__Pipeline_Product__c prod : products){
            string family = prod.V2SACL1__Family__c;
            if(mapPlatforms.containsKey(family) && !lookupTypeList.contains(family)){
                mapPlatforms.get(prod.V2SACL1__Family__c).products.add(prod);
            } else {
                PipelineProductsByPlatform prodPlat = new PipelineProductsByPlatform(family);
                if(!lookupTypeList.contains(family)){
                    prodPlat.products.add(prod);
                }
                mapPlatforms.put(family, prodPlat);
            }
        }
             
        //return platformProducts;
        return mapPlatforms;
    }
    /**
    * @description This method returns the metadata needed in product selector
    * Including columns to show and a list of products grouped by platform
    */
    @AuraEnabled
    public static Metadata getMetadata(Id opportunityId ){        
        Metadata meta = new Metadata();
              
        meta.fields = getFieldsMetadata();
        meta.prodsByPlatform = getProductsListByPlatform(opportunityId);
        
        return meta;
    }
    
    /**
    * @description This method is used to save changes to PipelineDetails
    */
    @AuraEnabled
    public static void savePipelineDetails(String pipelineDetailsToUpsertJson){        
        List<V2SACL1__Pipeline_Detail__c> pipelineDetailsToUpsertList = pipelineDetailsToUpsertJson != '' ? 
            (List<V2SACL1__Pipeline_Detail__c>)System.JSON.deserializeStrict(pipelineDetailsToUpsertJson, List<V2SACL1__Pipeline_Detail__c>.Class) : new List<V2SACL1__Pipeline_Detail__c>();
        
        if(pipelineDetailsToUpsertList.size() > 0) {
            try {
                upsert(pipelineDetailsToUpsertList);
            } catch (Exception ex) {
                //throw new AuraException(ex.getMessage());
                String errorMsg = ex.getMessage();
                if(errorMsg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                    errorMsg = errorMsg.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ', ': ');
                }
                throw new AuraHandledException(errorMsg);
            }
            
        }
    }
    
    /**
    * @description Generic function to return possible options for a picklist field
    * @param describeFieldResult picklist field to get the options from
    * @return possible values in a LocalSelect form
    */
    public static List<LocalSelect> getPicklistLabels(Schema.DescribeFieldResult describeFieldResult){
    	List<LocalSelect> pickListLabels= new List<LocalSelect>();
        
        for(Schema.PicklistEntry pickListVal : describeFieldResult.getPicklistValues()){            
			pickListLabels.add(new LocalSelect(pickListVal.getLabel(), pickListVal.getLabel()));
		}     
        
        return pickListLabels;
    }
    
    /**
	* @description Get possible Status options for OpportunityLineItem
    */
    @AuraEnabled
    public static List<LocalSelect> getPipelineDetailStatusOptions(){
        return getPicklistLabels(SObjectType.V2SACL1__Pipeline_Detail__c.fields.V2SACL1__Status__c );
    }
    
    /**
	* @description Get possible Sub-Category options for OpportunityLineItem
    */
    @AuraEnabled
    public static List<LocalSelect> getPipelineDetailSubCategoryOptions(){
        return getPicklistLabels(SObjectType.V2SACL1__Pipeline_Detail__c.fields.V2SACL1__Sub_Category__c);
    }

    @AuraEnabled
    public static User getUserInfo(Id userId){
        return [SELECT Id, LastName, Profile.Name FROM User WHERE Id = : userId LIMIT 1];
    }

    @AuraEnabled
    public static List<V2SACL1__Prod_Sel_Permissions__mdt> getPermissions(){
        string profileName = [SELECT Id, Name FROM Profile WHERE Id =: UserInfo.getProfileId() LIMIT 1].Name;
        List<V2SACL1__Prod_Sel_Permissions__mdt> permissionList = [SELECT V2SACL1__Username__c, V2SACL1__Profile_Name__c, V2SACL1__IsUser__c, V2SACL1__Is_Active__c, V2SACL1__Mark_booked_button_enabled__c, V2SACL1__Allow_Select_Previous_Months__c FROM V2SACL1__Prod_Sel_Permissions__mdt WHERE V2SACL1__Is_Active__c = true AND V2SACL1__Username__c =: UserInfo.getUserName()];

        if (permissionList.size() == 0) {
            permissionList = [SELECT V2SACL1__Username__c, V2SACL1__Profile_Name__c, V2SACL1__IsUser__c, V2SACL1__Is_Active__c, V2SACL1__Mark_booked_button_enabled__c, V2SACL1__Allow_Select_Previous_Months__c FROM V2SACL1__Prod_Sel_Permissions__mdt WHERE V2SACL1__Is_Active__c = true AND V2SACL1__Profile_Name__c =: profileName];
        }
        system.debug('permissions');
        system.debug(permissionList);
        return permissionList;
    }
     
}