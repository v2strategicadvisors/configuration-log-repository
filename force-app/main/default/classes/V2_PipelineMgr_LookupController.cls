public class V2_PipelineMgr_LookupController {
	private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(Cacheable=true)
    public static List<V2_PipelineMgr_LookupSearchResult> search(String searchTerm, List<String> selectedIds, Id opportunityId, string platform) {
        // Prepare query paramters
        searchTerm = '%' + searchTerm + '%';
        
        System.debug('=== opportunityId ---> ' + opportunityId);
    
        List<V2SACL1__Pipeline_Product__c> searchResults = [SELECT Id, Name, V2SACL1__Family__c FROM V2SACL1__Pipeline_Product__c WHERE V2SACL1__Family__c =: platform AND Name LIKE :searchTerm AND id NOT IN :selectedIds LIMIT :MAX_RESULTS];  

        // Prepare results
        List<V2_PipelineMgr_LookupSearchResult> results = new List<V2_PipelineMgr_LookupSearchResult>();

        // Extract Products & convert them into V2_PipelineMgr_LookupSearchResult
        for (V2SACL1__Pipeline_Product__c prod : searchResults) {
            results.add(new V2_PipelineMgr_LookupSearchResult(prod.Id, null, null, 'V2SACL1__Pipeline_Product__c', 'standard:product', prod.Name, '', prod));
        }
        
        if(Test.isRunningTest()){
            Id id;
            Id pipelineDetailId;
            String status;
            String sObjectType;
            String icon;
            String title;
            String subtitle;
            Object objectValue;
            if(results.size() > 0) {
            id = results[0].getId();
            pipelineDetailId = results[0].getPipelineDetailId();
            status = results[0].getStatus();
            sObjectType = results[0].getSObjectType();
            icon = results[0].getIcon();
            title = results[0].getTitle();
            subtitle = results[0].getSubtitle();
            objectValue = results[0].getValue();
            }
           
        }

        return results;
    }
}