/*
 * @Description: This Controller Class contains all the main functionalities 
 * 
 * Abbreviations:
 * - QF: previously Quarterly Forecast now Quarterly Account Estimate
 * - BFS: Bottoms Up Forecast Snapshot now Account Estimate Snapshot
 * - PY: Previous Year
 */
public with sharing class V2_AccEstimatesManager { 
    static List<String> channelsList = getChannelPicklistValues();
    /*
    * INNER CLASSES
    */
    public class ImproperYearException extends Exception {}
    
    public class FilterOption {
        @AuraEnabled public List<LocalSelect> listLocalSelect {get; set;}
    }

    public class LocalSelect {
       @AuraEnabled
       public string value {get; set;}
       @AuraEnabled
       public string label {get; set;}
       public LocalSelect(string value, string label){
           this.value = value;
           this.label = label;
       }
    }
    
    public class ChannelWithTotals {
        @AuraEnabled public string channelName {get; set;}
        @AuraEnabled public decimal baseYearTotal {get; set;}
        @AuraEnabled public decimal prevToBYActualsTotal {get; set;}
        
        public ChannelWithTotals(string channelName){
            this.channelName = channelName;
            this.baseYearTotal = 0;
            this.prevToBYActualsTotal = 0;
        }
    }           
    
    public class ChannelWithQFWrapper {
        @AuraEnabled public string channelName {get; set;}
        @AuraEnabled public Quarterly_Account_Estimate__c selYearQF {get; set;}
    }    

    /*
    * WRAPPERS
    */
    public class SnapshotWrapper {
        @AuraEnabled public Account_Estimate_Snapshot__c BFS {get; set;}
        @AuraEnabled public decimal quarterTotal {get; set;}
		@AuraEnabled public decimal PYActualsTotPerQuarter {get; set;}
        @AuraEnabled public List<ChannelWithQFWrapper> listChannQFs {get; set;}
        
        //There should be as many listChannQuarterForecasts as there are channels defined in the channel__c 
        //picklist in quarterly_forecast__c 
        //SHOULD be ordered in the same way getChannelPicklistValues is 
        public SnapshotWrapper(Account_Estimate_Snapshot__c BFS){
            this.BFS = BFS;
            this.quarterTotal = 0;
            this.PYActualsTotPerQuarter = 0;
            this.listChannQFs = new List<ChannelWithQFWrapper>();         
        }
    }

    public class AccountWithSnapshotsWrapper {
        @AuraEnabled public String accountId {get; set;}
        @AuraEnabled public String accountName {get; set;}
        @AuraEnabled public List<ChannelWithTotals> totalsByChannel {get; set;}
        
        @AuraEnabled public decimal baseYearTotal {get; set;} 
        @AuraEnabled public decimal prevToBYActualsTotal {get; set;} 
        //There should be four of these, one per quarter
        @AuraEnabled public List<SnapshotWrapper> listPlanSnapshots {get; set;}
        
        public AccountWithSnapshotsWrapper(){
            this.totalsByChannel = new List<ChannelWithTotals>();
            this.baseYearTotal = 0; 
        	this.prevToBYActualsTotal = 0;
            this.listPlanSnapshots = new List<SnapshotWrapper>();
        
            for(string chann : channelsList){
                ChannelWithTotals chanTot = new ChannelWithTotals(chann);
              
                this.totalsByChannel.add(chanTot);
            }			
        }
    }
    
    public class EstimateSummaryData {
        //There should be one per account that will be shown
        @AuraEnabled public List<AccountWithSnapshotsWrapper> listAccWithSnapwrapper {get; set;}
        
        //There should be 4 one per quarter (in the same order of the quarters shown in the UI)
        @AuraEnabled public List<decimal> selYearEstimatesTotalPerQuartList {get; set;}

        //There should be 4 one per quarter (in the same order of the quarters shown in the UI)
        @AuraEnabled public List<decimal> prevYearActualsTotalPerQuartList {get; set;}

        @AuraEnabled public decimal baseYearTotal {get; set;}
        @AuraEnabled public decimal prevToBYActualsTotal {get; set;}
        @AuraEnabled public boolean showViewMore {get; set;}
                
        public EstimateSummaryData(){
            this.selYearEstimatesTotalPerQuartList = new List<decimal>();            
            this.prevYearActualsTotalPerQuartList = new List<decimal>();
            for(integer i = 0; i<4; i++){
                selYearEstimatesTotalPerQuartList.add(0);
                prevYearActualsTotalPerQuartList.add(0);
            }
            this.baseYearTotal = 0;
            this.prevToBYActualsTotal = 0;
        }
        public EstimateSummaryData(List<AccountWithSnapshotsWrapper> listAccWithSnapwrapper) {
            this.listAccWithSnapwrapper = listAccWithSnapwrapper;
        }
    }
    
    private static AccountWithSnapshotsWrapper initializeAccSnapsWrap(Account acc) {
        AccountWithSnapshotsWrapper accSnapsWrap = new AccountWithSnapshotsWrapper();
        accSnapsWrap.accountId = acc.Id;
        accSnapsWrap.accountName = acc.Name;
        return accSnapsWrap;
    }
   
    @AuraEnabled
    public static List<String> getChannelPicklistValues(){
		List<String> listChannels = new List<String>();
        Schema.DescribeFieldResult fieldResult = Quarterly_Account_Estimate__c.Channel__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry val : ple){
            listChannels.add(val.getValue()); 
        }
        
        return listChannels;
    }

    @AuraEnabled
    public static FilterOption getFilterOptions(){
        FilterOption newFilterOption = new FilterOption();
        newFilterOption.listLocalSelect = new List<LocalSelect>();
        
        newFilterOption.listLocalSelect.add(new LocalSelect('My Estimated Accounts', 'My Estimated Accounts'));
        
        for(Account_Filter__c accFilter : [SELECT Id, Name FROM Account_Filter__c LIMIT 1000]){
            newFilterOption.listLocalSelect.add(new LocalSelect(accFilter.Id, accFilter.Name));
        }
        
        return newFilterOption;
    }

    private static List<Account_Filter__c> getAccFilter(string accountList){
        return [SELECT id, Name, Account_Name_Contains__c, Account_Industry__c, Account_Type__c, My_Estimated_Accounts__c,
        AES_year__c, Snapshot_Owner__c, Snapshot_Owner_Office__c FROM Account_Filter__c WHERE id =: accountList];
    }

    private static Map<String, Account> getFilteredAccounts(string accountList, string accountName, Account_Filter__c accountFilter,
                                                            integer offset,integer paginationSize, Set<string> years){ 
             
        Boolean isMyAccount = false;
        String query = 'SELECT ';
        query       += ' Id,';
        query       += ' Name';
        query       += ' FROM Account Where Id != null ';
        
        //Add Conditional Filters
        if(accountName != ''){
            String queryParam = '%' + string.escapeSingleQuotes(accountName) + '%';
            query += ' AND Name LIKE ' + '\'' + queryParam + '\'';
        }
        else if(accountList == 'My Estimated Accounts'){
            isMyAccount = true;
        	query += ' AND Id in (select Advertiser__c from Account_Estimate_Snapshot__c where CurrentUserIsSalesRep__c = true and V2SACL1__Year__c in: years)';
        }
        else { 
            
            if(accountFilter != null){
                boolean filterByAccountIds = false;
                
                if(accountFilter.Account_Industry__c != '' && accountFilter.Account_Industry__c != null){
                    query += ' AND Industry =\''+accountFilter.Account_Industry__c+'\'';
                }
                
                if(accountFilter.Account_Type__c != '' && accountFilter.Account_Type__c != null){
                    query += ' AND Type =\''+accountFilter.Account_Type__c+'\'';
                }
                
                if(accountFilter.My_Estimated_Accounts__c){
                    isMyAccount = true;
        			query += ' AND Id in (select Advertiser__c from Account_Estimate_Snapshot__c where CurrentUserIsSalesRep__c = true and V2SACL1__Year__c in: years)';
                }
                
                if(accountFilter.Account_Name_Contains__c  != '' && accountFilter.Account_Name_Contains__c  != null){
                    query += ' AND Name LIKE \'%' + accountFilter.Account_Name_Contains__c + '%\'';
                }
                
                if(accountFilter.AES_year__c != null){
                    query += ' AND Id in (select Advertiser__c from Account_Estimate_Snapshot__c where Year__c =\''+
                        accountFilter.AES_year__c+'\')';
                }
                
                if(accountFilter.Snapshot_Owner__c != null){
                    query += ' AND Id in (select Advertiser__c from Account_Estimate_Snapshot__c where Sales_Rep_1__r.Name like \'%'+
                        accountFilter.Snapshot_Owner__c+'%\')';
                }
            }
        }
                
        query += ' ORDER BY Name Limit :paginationSize OFFSET :offset';

        
        Map<String, Account> listAccounts = new Map<String, Account>(
            (List<Account>) Database.query(query)
        );
        
        if(isMyAccount){
            Account newBusiness = new Account(Name = 'New Business');
        	listAccounts.put(null,newBusiness);
        }
  
        return listAccounts;     
    }
    
    private static Quarterly_Account_Estimate__c initializeQuarterlyForecast(integer quarter){
        Quarterly_Account_Estimate__c quaForecast = new Quarterly_Account_Estimate__c();
        quaForecast.Quarter__c = 'Q'+quarter;
        
        return quaForecast;
    }
                 
    private static List<ChannelWithQFWrapper> getChannelsEstimates (Account_Estimate_Snapshot__c FS, integer quarter, 
        Map<String, Quarterly_Account_Estimate__c> mapQFPerAccQuarChan){  
            
        List<ChannelWithQFWrapper> chanWithWraps = new List<ChannelWithQFWrapper>();
            
        for(string chan : channelsList){
            ChannelWithQFWrapper chanWithQF = new ChannelWithQFWrapper();
            chanWithQF.channelName = chan;
            
            Quarterly_Account_Estimate__c selYearFor = mapQFPerAccQuarChan.get(FS.Id+'-Q'+quarter+'-'+chan);
                
            chanWithQF.selYearQF = selYearFor != null ? selYearFor : initializeQuarterlyForecast(quarter) ;
            
            chanWithWraps.add(chanWithQF);
        }
                                                                  
        return chanWithWraps;
    }
    
    private static void getStaticCurrYearAndPrevTotal(Account acc, 
        Map<String, Account_Estimate_Snapshot__c> mapPlanSnaps, AccountWithSnapshotsWrapper accWithSnapsWrap,
        Map<String, Quarterly_Account_Estimate__c> mapQuaForePerAccQuarChan, integer baseYear){
            
        
        Account_Estimate_Snapshot__c baseYearAPS = mapPlanSnaps.containsKey(acc.Id+'-'+baseYear) ? 
                mapPlanSnaps.get(acc.Id+'-'+baseYear) : new Account_Estimate_Snapshot__c();
         //System.debug('baseYearAPS: ' + baseYearAPS);
		System.debug('output: ' + (mapPlanSnaps.containsKey(acc.Id+'-'+baseYear) ? 
                mapPlanSnaps.get(acc.Id+'-'+baseYear) : new Account_Estimate_Snapshot__c()));
        if(baseYearAPS.Id != null){
            integer counter = 0;
            for(string chann : channelsList){
                ChannelWithTotals chanTot = new ChannelWithTotals(chann);
                for(integer i = 0; i<4; i++){
                    Quarterly_Account_Estimate__c baseYQuart = mapQuaForePerAccQuarChan.get(baseYearAPS.Id+'-Q'+(i+1)+'-'+chann);
                    
                    chanTot.baseYearTotal += baseYQuart != null ? baseYQuart.Quarterly_Estimate__c : 0;
                   
                    chanTot.prevToBYActualsTotal += baseYQuart != null && baseYQuart.Previous_Year_Actual__c != null
                        ? baseYQuart.Previous_Year_Actual__c : 0;
                    
                }	
                
                accWithSnapsWrap.baseYearTotal += chanTot.baseYearTotal; 
                accWithSnapsWrap.prevToBYActualsTotal += chanTot.prevToBYActualsTotal; 
                
                accWithSnapsWrap.totalsByChannel[counter] = chanTot;
                counter++;
            }			
        }
    }
    
    @AuraEnabled
    public static EstimateSummaryData getEstimateSummaryData(integer year, integer quarter, 
        string accountList, string accountName, integer offset, integer paginationSize) {
        
            if(year!= null){
                List<Account_Filter__c> accFilters = getAccFilter(accountList);
                Account_Filter__c accountFilter = accFilters.size() > 0 ? accFilters[0] : null;
                Set<string> years = new Set<string>();
                integer quarterCount = quarter;
                integer yearCount = year;
                    
                years.add(String.valueOf(yearCount));
                    
                for(integer i=0;i<4;i++){
                    quarterCount++;
                    if(quarterCount == 5){
                         quarterCount = 1;
                         yearCount++;
                        years.add(String.valueOf(yearCount));
                    }
                }
                //get filtered accounts
                Map<String, Account> filteredAccounts = getFilteredAccounts(accountList, accountName, 
                                                                            accountFilter, offset, paginationSize, years);
                      
                if(filteredAccounts.size()==0){
                     return new EstimateSummaryData(new List<AccountWithSnapshotsWrapper>());
                }
                    
                EstimateSummaryData estSummaryToReturn = new EstimateSummaryData();
                 
                Map<String, Account> accountsAvailAfterOffset = getFilteredAccounts(accountList, accountName, accountFilter, offset+paginationSize, 1, years);
                    
                boolean isMyAccount = accountList == 'My Estimated Accounts' || (accountFilter != null && accountFilter.My_Estimated_Accounts__c);   
                    
                estSummaryToReturn.showViewMore = (accountsAvailAfterOffset.size() == 1 && !isMyAccount) || 
                    (accountsAvailAfterOffset.size() > 1 && isMyAccount);
                    
                //get only ids from those filtered accounts
                Set<String> filteredAccountIds = filteredAccounts.keySet();
                    
                Quarterly_Estimate_Setting__c defaultSettings = getDefaultSettings();
                integer baseYear = integer.valueOf(defaultSettings.Base_Year__c);
                List<Account_Estimate_Snapshot__c> listSnaps = getListSnapshotsWithQuarters(year, quarter, filteredAccountIds, baseYear);
                 
                 
                Map<String, Account_Estimate_Snapshot__c> mapPlanSnaps = new Map<String, Account_Estimate_Snapshot__c>();
                Map<String, decimal> mapTotalPerAccQuarter = new Map<String, decimal>();
                Map<String, decimal> mapTotalPYActualsPerAccQuarter = new Map<String, decimal>();
                Map<String, Quarterly_Account_Estimate__c> mapQuaForePerAccQuarChan = new Map<String, Quarterly_Account_Estimate__c>();
                    
                for(Account_Estimate_Snapshot__c snap: listSnaps) {
                    mapPlanSnaps.put(snap.Advertiser__c + '-' + snap.year__c, snap);
                    //System.debug('testing: ' + snap.Advertiser__c + '-' + snap.year__c);
                    //System.debug('snap: ' + snap);
                    
                    for(Quarterly_Account_Estimate__c quaFore : snap.Quarterly_Account_Estimates__r) {
                        if(mapTotalPerAccQuarter.containsKey(snap.Id+'-'+quaFore.Quarter__c)){
                            mapTotalPerAccQuarter.put(snap.Id+'-'+quaFore.Quarter__c,  
                                                      mapTotalPerAccQuarter.get(snap.Id+'-'+quaFore.Quarter__c) + 
                                                      quaFore.Quarterly_Estimate__c);
                        }else{
                            mapTotalPerAccQuarter.put(snap.Id+'-'+quaFore.Quarter__c, quaFore.Quarterly_Estimate__c);
                        }
        
                        if(mapTotalPYActualsPerAccQuarter.containsKey(snap.Id+'-'+quaFore.Quarter__c)){
                            decimal prevYearActuals = quaFore.Previous_Year_Actual__c == null ? 0 :quaFore.Previous_Year_Actual__c;
                            mapTotalPYActualsPerAccQuarter.put(snap.Id+'-'+quaFore.Quarter__c,  
                                                      mapTotalPYActualsPerAccQuarter.get(snap.Id+'-'+quaFore.Quarter__c) + 
                                                      prevYearActuals);
                           
                        }else{
                            decimal prevYearActuals = quaFore.Previous_Year_Actual__c == null ? 0 :quaFore.Previous_Year_Actual__c;
                            mapTotalPYActualsPerAccQuarter.put(snap.Id+'-'+quaFore.Quarter__c, prevYearActuals);
                        }
                        
                        if(!mapQuaForePerAccQuarChan.containsKey(snap.Id+'-'+quaFore.Quarter__c+'-'+quaFore.channel__c)){
                            mapQuaForePerAccQuarChan.put(snap.Id+'-'+quaFore.Quarter__c+'-'+quaFore.channel__c,quaFore);
                        }
                        
                    }
                }
                    
                List<AccountWithSnapshotsWrapper> accWithSnapsWrapList = new List<AccountWithSnapshotsWrapper>();
                    
                for(Account acc: filteredAccounts.values()){
                    AccountWithSnapshotsWrapper accWithSnapsWrap = initializeAccSnapsWrap(acc);
                    
                    integer quarterCounter = quarter;
                    integer yearCounter = year;
                    
                    for(integer i=0;i<4;i++){
                        Account_Estimate_Snapshot__c newAccSnap = new Account_Estimate_Snapshot__c();
                        newAccSnap.Year__c = string.valueof(yearCounter);
                        
                        Account_Estimate_Snapshot__c selYearAPS = mapPlanSnaps.containsKey(acc.Id+'-'+yearCounter) ? 
                            mapPlanSnaps.get(acc.Id+'-'+yearCounter) : newAccSnap;
        
                        SnapshotWrapper snapWrapper = new SnapshotWrapper(selYearAPS);
                        decimal selecYearEstTotalPerQuarter = mapTotalPerAccQuarter.get(selYearAPS.Id+'-Q'+quarterCounter);
                        decimal PYActualsTotPerQuarter =  mapTotalPYActualsPerAccQuarter.get(selYearAPS.Id+'-Q'+quarterCounter);
                        
                        snapWrapper.quarterTotal = selecYearEstTotalPerQuarter != null ? selecYearEstTotalPerQuarter : 0;
                        snapWrapper.PYActualsTotPerQuarter = PYActualsTotPerQuarter != null ? PYActualsTotPerQuarter : 0; 
                        
                        snapWrapper.listChannQFs = 
                            getChannelsEstimates(selYearAPS, 
                                                 quarterCounter, mapQuaForePerAccQuarChan);
                                                 
                        accWithSnapsWrap.listPlanSnapshots.add(snapWrapper);
                        quarterCounter++;
                        if(quarterCounter == 5){
                            quarterCounter = 1;
                            yearCounter++;
                        }
                            
                        estSummaryToReturn.selYearEstimatesTotalPerQuartList[i] += snapWrapper.quarterTotal;
                       
                        estSummaryToReturn.prevYearActualsTotalPerQuartList[i] += snapWrapper.PYActualsTotPerQuarter;
                    }
                    
                    getStaticCurrYearAndPrevTotal(acc, mapPlanSnaps, accWithSnapsWrap, mapQuaForePerAccQuarChan, baseYear);
                    
                    estSummaryToReturn.baseYearTotal += accWithSnapsWrap.baseYearTotal;
                    estSummaryToReturn.prevToBYActualsTotal += accWithSnapsWrap.prevToBYActualsTotal;
                    
                    accWithSnapsWrapList.add(accWithSnapsWrap);
                }
            
                estSummaryToReturn.listAccWithSnapwrapper = accWithSnapsWrapList;
                
                return estSummaryToReturn;
            }
        
        return new EstimateSummaryData(new List<AccountWithSnapshotsWrapper>());
        
    }
    
    private static List<Account_Estimate_Snapshot__c> getListSnapshotsWithQuarters(integer year, integer quarter, 
                                                                                      Set<String> accountIds, integer baseYear) {
        Id currUserId = userinfo.getuserid();
        Set<string> yearsToSearch = new Set<string>();
        yearsToSearch.add(string.valueof(year-1));
        yearsToSearch.add(string.valueof(year));
        yearsToSearch.add(string.valueof(baseYear));
        
        if(quarter != 1){
        	yearsToSearch.add(string.valueof(year+1));    
        }
        
        String query = 'SELECT ';
        query       += ' Id,';
        query       += ' Name,';
        query       += ' Advertiser__c,';
        query       += ' Advertiser__r.Name,';
        query       += ' Year__c,';
        query       += ' (SELECT Id, Locked__c, Channel__c, Quarterly_Estimate__c, Quarter__c, Previous_Year_Actual__c FROM Quarterly_Account_Estimates__r)';
        query       += ' FROM Account_Estimate_Snapshot__c where year__c in:yearsToSearch'+
            		   ' and ((Advertiser__c = null and Sales_Rep_1__c =: currUserId) or (Advertiser__c IN : accountIds and Advertiser__c != null))';
        
        //Order By
        query += ' ORDER BY Advertiser__r.Name, Advertiser__c';
        
        List<Account_Estimate_Snapshot__c> listPlans = Database.query(query);
       
        return listPlans;
    }
    
	@AuraEnabled
    public static Quarterly_Account_Estimate__c save(string quarterForecastJson, 
                                             string year, string channel, string accountId){                                         
        Id currUserId = userinfo.getuserid();
              
        Quarterly_Account_Estimate__c quaFore = (Quarterly_Account_Estimate__c)System.JSON.deserializeStrict(quarterForecastJson, 
                                           Quarterly_Account_Estimate__c.Class);
        
        if(quaFore.quarterly_Estimate__c == null)
            return quaFore;
           
        if(quaFore.Id == null){
            //Figure out there is an estimate snapshot for the current year paremeter
            //Insert one if not
            
            List<Account_Estimate_Snapshot__c> exisBFSList = new List<Account_Estimate_Snapshot__c>();
                      
            if(accountId == null){
                exisBFSList = [select Id from Account_Estimate_Snapshot__c where year__c =: year 
                                                          and
                                                          (Advertiser__c = null and Sales_Rep_1__c=:currUserId)];
            } else {
                exisBFSList = [select Id from Account_Estimate_Snapshot__c where year__c =: year 
                                                          and advertiser__c =:accountId];
            
            }
                           
            Account_Estimate_Snapshot__c exisBFS;
                
            if(exisBFSList.size() == 0){
                if(Schema.sObjectType.Account_Estimate_Snapshot__c.fields.year__c.isCreateable() &&
                   Schema.sObjectType.Account_Estimate_Snapshot__c.fields.Sales_Rep_1__c.isCreateable() &&
                   Schema.sObjectType.Account_Estimate_Snapshot__c.fields.Advertiser__c.isCreateable() &&
                   Schema.sObjectType.Account_Estimate_Snapshot__c.isCreateable()){
                   	exisBFS = new Account_Estimate_Snapshot__c();
              
                    exisBFS.year__c = year;
                    exisBFS.Sales_Rep_1__c = currUserId;
                    exisBFS.Advertiser__c = accountId;
                    insert exisBFS;
                }
                
            } else {
                exisBFS = exisBFSList[0];
            }
            
            if(Schema.sObjectType.Quarterly_Account_Estimate__c.fields.Account_Estimate_Snapshot__c.isCreateable() &&
              Schema.sObjectType.Quarterly_Account_Estimate__c.fields.Channel__c.isCreateable() &&
              Schema.sObjectType.Quarterly_Account_Estimate__c.isCreateable()){
                quaFore.Account_Estimate_Snapshot__c = exisBFS.Id;
            	quaFore.Channel__c = channel;
                insert quaFore;
            }
        }
		
        if(Schema.sObjectType.Quarterly_Account_Estimate__c.fields.Account_Estimate_Snapshot__c.isUpdateable() &&
              Schema.sObjectType.Quarterly_Account_Estimate__c.fields.Channel__c.isUpdateable() &&
              Schema.sObjectType.Quarterly_Account_Estimate__c.isUpdateable()){
        	update quaFore;    
        }
              
        return [Select Id, Locked__c, Account_Estimate_Snapshot__c, Channel__c, Quarterly_Estimate__c, Quarter__c, Previous_Year_Actual__c
                from Quarterly_Account_Estimate__c where Id =: quaFore.Id LIMIT 1];
    }
    
    @AuraEnabled
    public static Quarterly_Estimate_Setting__c getDefaultSettings(){
    	List<Quarterly_Estimate_Setting__c> estimatesSettings = Quarterly_Estimate_Setting__c.getall().values();
      
        if(estimatesSettings.size() > 0) {
            try
            {                
                integer baseYear = estimatesSettings[0].Base_Year__c != null ? 
                    integer.valueof(estimatesSettings[0].Base_Year__c) : System.Today().year();
                                
                estimatesSettings[0].Base_Year__c = String.valueof(baseYear);
                estimatesSettings[0].Red_Limit__c = estimatesSettings[0].Red_Limit__c != null ? 
                    estimatesSettings[0].Red_Limit__c : 0;
                estimatesSettings[0].Yellow_Limit__c = estimatesSettings[0].Yellow_Limit__c != null ? 
                    estimatesSettings[0].Yellow_Limit__c : 15;
                
               	return estimatesSettings[0];
            }
            catch(System.TypeException te){
                throw new ImproperYearException('Check Base Year Setting and make sure it is set to a proper year.');
            }
            
        }
        Quarterly_Estimate_Setting__c QES = new Quarterly_Estimate_Setting__c();
        QES.Red_Limit__c = 0;
        QES.Yellow_Limit__c = 15;
        QES.Base_Year__c = String.valueof(System.Today().year());
        return QES;
    }
    
}