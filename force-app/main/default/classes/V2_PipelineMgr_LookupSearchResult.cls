/*
* @Description: Class used to serialize a single Lookup search result item.
* The Lookup controller returns a List<V2_PipelineMgr_LookupSearchResult> when sending search result back to Lightning Web Components
*/
public class V2_PipelineMgr_LookupSearchResult {
    private Id id;
    private Id pipelineDetailId;
    private string status;
    private String sObjectType;
    private String icon;
    private String title;
    private String subtitle;
	private Object objectValue;
        
    public V2_PipelineMgr_LookupSearchResult(Id id, Id pipelineDetailId, String status, String sObjectType, String icon, String title, String subtitle, Object objectValue) {
        this.id = id;
        this.pipelineDetailId = pipelineDetailId;
        this.status = status;
        this.sObjectType = sObjectType;
        this.icon = icon;
        this.title = title;
        this.subtitle = subtitle;
        this.objectValue = objectValue;
    }

    @AuraEnabled
    public Id getId() {
        return id;
    }
    
    @AuraEnabled
    public Id getPipelineDetailId() {
        return pipelineDetailId;
    }

    @AuraEnabled
    public String getStatus() {
        return status;
    }
    
    @AuraEnabled
    public String getSObjectType() {
        return sObjectType;
    }

    @AuraEnabled
    public String getIcon() {
        return icon;
    }

    @AuraEnabled
    public String getTitle() {
        return title;
    }

    @AuraEnabled
    public String getSubtitle() {
        return subtitle;
    }
    
    @AuraEnabled
    public Object getValue() {
        return objectValue;
    }

}