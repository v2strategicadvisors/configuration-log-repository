/** 
* This test class contains unit tests for validating the behavior of V2_ProSel_LookupController.cls 
controller used in v2ProductSelectorMain Lightning web component
* 
* @autor V2 Strategic Advisor (v2analyst@v2sa.com) - developer: Fernando Ortiz
*/

@isTest(seeAllData = false)
public class V2_ProSel_LookupController_Test {
    static boolean doInsert = true;
    static integer numOfAcct = 1;
    static integer numOfOpp = 1;
    static integer numOfProduct = 40;
    
	static list<Opportunity> listOpps = new list<Opportunity>();
    static list<Product2> listProducts = new list<Product2>();
    static list<PricebookEntry> listStandardPricebookEntries = new List<PricebookEntry>();
    static Id standardpricebookId = Test.getStandardPricebookId();
    private static final List<string> familyProgramList = new List<string> {'Print', 'BrandVoice', 'Conference', 'Live'};
    
    /**
    * The purpose of this method is to create test data.
    */
    static void createTestData() {
        /*Create Account and Opportunity*/
        listOpps = V2SA_Util_TestDataFactory.createAcctsWithOppsList(numOfAcct, numOfOpp, null, null, false);
        for (Integer i = 0;i < numOfOpp;i++){
        	listOpps[i].Pricebook2Id = standardpricebookId;
        }
        insert listOpps;
                
        /*Create Products*/
        listProducts = V2SA_Util_TestDataFactory.generateObjectList('Product2', numOfProduct, null, false);
        for (Integer i = 0;i < numOfProduct;i++){
            if (Math.mod(i,5) == 0) {
                listProducts[i].Family = 'Print';
            } else if (Math.mod(i,5) == 1) {
                listProducts[i].Family = 'BrandVoice';
            } else if (Math.mod(i,5) == 2) {
                listProducts[i].Family = 'Conference';
            } else {
                listProducts[i].Family = 'Live';
            }
            listProducts[i].IsActive = true;
            listProducts[i].V2SACL1__Month_Issue_Close_Date__c = date.today();
        }
        System.debug('***** listProducts ---> ' + listProducts);
        insert listProducts;
        
        /*Add Products to Standard Pricebook*/
        for(Product2 objProduct : listProducts) {
            listStandardPricebookEntries.add(new PricebookEntry(Pricebook2Id = standardpricebookId, 
                                                                Product2Id = objProduct.Id, UnitPrice = 25000, 
                                                                isActive = true)); 
        }
        insert listStandardPricebookEntries;  
        
        Opportunity currentOpp = listOpps[0];
        
        Opportunity updtOpp = [select Id, pricebook2Id from Opportunity where Id =: currentOpp.Id limit 1];
                
       	updtOpp.pricebook2Id =  listStandardPricebookEntries[0].pricebook2Id;
        update updtOpp;
            
    }
    
    
    static testMethod void testPositiveSearchShouldReturnProduct(){
        //Create test data
      	createTestData();
        
        Opportunity currentOpp = listOpps[0];
        
        String unitTestName = 'testPositiveSearchShouldReturnProduct';
                        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getDMLStatements());
        
        system.Test.startTest();
        List<V2_ProdSel_LookupSearchResult> searchResult = V2_ProSel_LookupController.search('Test Product', new List<string>(), currentOpp.Id, 'Print');
        System.debug('+++ Result ---> ' + searchResult);
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getDMLStatements());
        system.Test.StopTest();

        //ASSERTS
        System.assert(searchResult.size() > 0, 'No products found.');
        
        
        
        
    }
    
}