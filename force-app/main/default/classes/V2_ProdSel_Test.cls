/** 
* This test class contains unit tests for validating the behavior of V2_ProdSel.cls 
controller used in v2ProductSelectorMain Lightning web component
* 
* @autor V2 Strategic Advisor (v2analyst@v2sa.com) - developer: Brenda Taveras
*/
@isTest(seeAllData = false) 
public class V2_ProdSel_Test { 
    static boolean doInsert = true;
    static integer numOfAcct = 1;
    static integer numOfOpp = 1;
    static integer numOfProduct = 50;
    
    static User standardUser;
    static list<Opportunity> listOpps = new list<Opportunity>();
    static list<Product2> listProducts = new list<Product2>();
    static list<PricebookEntry> listStandardPricebookEntries = new List<PricebookEntry>();
    static Id standardpricebookId = Test.getStandardPricebookId();
    private static final List<string> familyProgramList = new List<string> {'Print', 'Conference', 'Live'};
    
          
    /**
    * The purpose of this method is to create test data.
    */
    static void createTestData() {
        /*Create Standard User*/
        standardUser = V2SA_Util_TestDataFactory.generateUserObject('Standard User',true,doInsert);
            
        /*Create Account and Opportunity*/
        listOpps = V2SA_Util_TestDataFactory.createAcctsWithOppsList(numOfAcct, numOfOpp, 'Advertiser / Brand', 'Cross-Platform Opportunity', false);
        for (Integer i = 0;i < numOfOpp;i++){
        	listOpps[i].Pricebook2Id = standardpricebookId;
        }
        insert listOpps;
                
        /*Create Products*/
        listProducts = V2SA_Util_TestDataFactory.generateObjectList('Product2', numOfProduct, null, false);
        for (Integer i = 0;i < numOfProduct;i++){
            if (Math.mod(i,5) == 0) {
                listProducts[i].Family = 'Print';
            } else if (Math.mod(i,5) == 1) {
                listProducts[i].Family = 'BrandVoice';
            } else if (Math.mod(i,5) == 2) {
                listProducts[i].Family = 'Conference';
            } else {
                listProducts[i].Family = 'Live';
            }
            listProducts[i].IsActive = true;
            listProducts[i].V2SACL1__Month_Issue_Close_Date__c = date.today();
        }
        
        insert listProducts;
        
        /*Add Products to Standard Pricebook*/
        for(Product2 objProduct : listProducts) {
            listStandardPricebookEntries.add(new PricebookEntry(Pricebook2Id = standardpricebookId, 
                                                                Product2Id = objProduct.Id, UnitPrice = 25000, 
                                                                isActive = true)); 
           
                
        }
        insert listStandardPricebookEntries;  
        
        Opportunity currentOpp = listOpps[0];
        
        Opportunity updtOpp = [select Id, pricebook2Id from Opportunity where Id =: currentOpp.Id limit 1];
                
       	updtOpp.pricebook2Id =  listStandardPricebookEntries[0].pricebook2Id;
        update updtOpp;
            
    }
       
	/** 
   	*  This test method contains tests for validating the behavior of saveOpportunityLineItems method
    *  where configuration contains:
	*      - 1 Account
  	*      - 1 Opportunity
  	*      - 50 Products
  	*      - 50 Price Book Entries
    *  SCENARIO:
    *      1. Create OLI object
    * 	   2. Save OLI through class method
    *  	   3. Assert OLI was correctly inserted
    */
    static testMethod void testPositiveSaveOpportunityLineItems() {
        //Create test data
      	createTestData();
        
        Opportunity currentOpp = listOpps[0];
        
        String unitTestName = 'testPositiveSaveOpportunityLineItems';
                        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getDMLStatements());
        system.Test.StartTest();
            
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = currentOpp.Id;
        oli.Quantity = 1;
        oli.UnitPrice = 1;
        oli.Description = 'sample';
        oli.PricebookEntryId = listStandardPricebookEntries[0].Id;
        
        oli.Product2Id = listStandardPricebookEntries[0].Product2Id;
    
        List<OpportunityLineItem> listOlis = new List<OpportunityLineItem>();
        listOlis.add(oli);
        String strOlis = JSON.serialize(listOlis);
        
        list<OpportunityLineItem> listOLIsToAssert = [SELECT Id, Description FROM OpportunityLineItem 
                                                      WHERE OpportunityId =: currentOpp.Id];
        system.assertEquals(0,listOLIsToAssert.size(),'Different number of saved OLI was expected.');
        
        V2_ProdSel.saveOpportunityLineItems(strOlis);
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getDMLStatements());
        system.Test.StopTest();

        //ASSERTS
        listOLIsToAssert = [SELECT Id, Description FROM OpportunityLineItem];
        system.assertEquals(1,listOLIsToAssert.size(),'Different number of saved OLI was expected.');
   
        system.debug(LoggingLevel.ERROR, 'SOQL query usage at the end of ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage at the end of ' + unitTestName + ' unit test: ' + Limits.getDMLStatements()); 
    }//testPositiveSaveOpportunityLineItems
         
    
    /** 
   	*  This test method contains tests for validating the behavior of saveOpportunityLineItems method when no OLI is passed
    *  SCENARIO:
    * 	   1. Call Save method with empty OLI patameter
    *  	   2. Assert no OLIs exist
    */
    static testMethod void testNegativeSaveOpportunityLineItems() {
        String unitTestName = 'testNegativeSaveOpportunityLineItems';
                        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getDMLStatements());
        system.Test.StartTest();

        List<OpportunityLineItem> listOlis = new List<OpportunityLineItem>();
        String strOlis = JSON.serialize(listOlis);
 
        V2_ProdSel.saveOpportunityLineItems(strOlis);
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getDMLStatements());
        system.Test.StopTest();

        //ASSERTS
        List<OpportunityLineItem> listOLIsToAssert = [SELECT Id, Description FROM OpportunityLineItem];
        system.assertEquals(0,listOLIsToAssert.size(),'Different number of saved OLI was expected.');
   
        system.debug(LoggingLevel.ERROR, 'SOQL query usage at the end of ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage at the end of ' + unitTestName + ' unit test: ' + Limits.getDMLStatements()); 
    }//testNegativeSaveOpportunityLineItems
    
    /** 
   	*  This test method contains tests for validating the behavior of getExistingOLIsByPlatform method
    *  where configuration contains:
	*      - 1 Account
  	*      - 1 Opportunity
  	*      - 50 Products
  	*      - 50 Price Book Entries
    *  SCENARIO:
    *      1. Create OLI object
    * 	   2. Insert OLI
    *  	   3. Assert OLI is returned by method and grouped by correct platform
    */
    static testMethod void testPositiveGetExistingOLIsByPlatform() {
        //Create test data
      	createTestData();
        
        Opportunity currentOpp = listOpps[0];
        
        String unitTestName = 'testPositiveGetExistingOLIsByPlatform';
                        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getDMLStatements());
        system.Test.StartTest();
            
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = currentOpp.Id;
        oli.Quantity = 1;
        oli.UnitPrice = 1;
        oli.Description = 'sample';
        oli.PricebookEntryId = listStandardPricebookEntries[0].Id;
        
        oli.Product2Id = listStandardPricebookEntries[0].Product2Id;
    
        List<OpportunityLineItem> listOlis = new List<OpportunityLineItem>();
        listOlis.add(oli);
        insert listOlis;
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getDMLStatements());
        system.Test.StopTest();

        //ASSERTS
        List<V2_ProdSel.OLIsByPlatform> olisByPlatform = V2_ProdSel.getExistingOLIsByPlatform(currentOpp.Id);
        
        system.assertEquals('Print', olisByPlatform[0].platform,'Different number of saved OLI was expected.');
        system.assertEquals(1, olisByPlatform[0].OLIs.size(),'Different number of saved OLI was expected.');
              
        system.debug(LoggingLevel.ERROR, 'SOQL query usage at the end of ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage at the end of ' + unitTestName + ' unit test: ' + Limits.getDMLStatements()); 
    }//testPositiveGetExistingOLIsByPlatform
    
    
    /** 
   	*  This test method contains tests for validating the behavior of getExistingOLIsByPlatform method
    *  where configuration contains:
	*      - 1 Account
  	*      - 1 Opportunity
  	*      - 50 Products
  	*      - 50 Price Book Entries
    *  SCENARIO:
    *      1. Call getExistingOLIsByPlatform method with invalid opportunityId
    * 	   2. Assert no OLI is returned
    */
    static testMethod void testNegativeGetExistingOLIsByPlatform() {
        String unitTestName = 'testNegativeGetExistingOLIsByPlatform';
        //ASSERTS
        List<V2_ProdSel.OLIsByPlatform> olisByPlatform = V2_ProdSel.getExistingOLIsByPlatform('00690000002BoMV');
        
        system.assertEquals(0, olisByPlatform.size(),'Different number of saved OLI was expected.');
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage at the end of ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage at the end of ' + unitTestName + ' unit test: ' + Limits.getDMLStatements()); 
    }//testNegativeGetExistingOLIsByPlatform
    
    /** 
   	*  This test method contains tests for validating the behavior of getOliStatusOptions method
    *  SCENARIO:
    *      1. Call getOliStatusOptions method
    *  	   2. Assert method returned values
    */
    static testMethod void testGetOliStatusOptions() {
        String unitTestName = 'testGetOliStatusOptions';
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getDMLStatements());
        system.Test.StartTest();
          
        List<V2_ProdSel.LocalSelect> oliStats = V2_ProdSel.getOliStatusOptions();
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getDMLStatements());
        system.Test.StopTest();

        //ASSERTS
        System.Assert(oliStats.size() > 0);
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage at the end of ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage at the end of ' + unitTestName + ' unit test: ' + Limits.getDMLStatements()); 
    }//testGetOliStatusOptions
    
    /** 
   	*  This test method contains tests for validating the behavior of getOliSubCategoryOptions method
    *  SCENARIO:
    *      1. Call getOliSubCategoryOptions method
    *  	   2. Assert method returned values
    */
    static testMethod void testGetOliSubCategoryOptions() {
        String unitTestName = 'testGetOliSubCategoryOptions';
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getDMLStatements());
        system.Test.StartTest();
          
        List<V2_ProdSel.LocalSelect> oliSubcats = V2_ProdSel.getOliSubCategoryOptions();
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getDMLStatements());
        system.Test.StopTest();

        //ASSERTS
        System.Assert(oliSubcats.size() > 0);
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage at the end of ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage at the end of ' + unitTestName + ' unit test: ' + Limits.getDMLStatements()); 
    }//testGetOliSubCategoryOptions
        
    /** 
   	*  This test method contains tests for validating the behavior of getMetadata method
    *  SCENARIO:
    *      1. Call getMetadata method
    *  	   2. Assert core columns are returned
    *      3. Assert products are correctly returned and grouped by platform
    */
    static testMethod void testPositiveGetMetadata() {
        //Create test data
      	createTestData();
        Opportunity currentOpp = listOpps[0];

        String unitTestName = 'testPositiveGetMetadata';
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getDMLStatements());
        system.Test.StartTest();
          
       	V2_ProdSel.Metadata metadat = V2_ProdSel.getMetadata(currentOpp.Id);
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getDMLStatements());
        system.Test.StopTest();

        //ASSERTS
        
        //Assert custom metadata types are returned
        System.Assert(metadat.fields.coreColumns.size() > 0);
        
        //Assert products are returned correctly and with the appropriate family
        system.debug('+++ familyProgramList ---> ' + familyProgramList);
        Integer counter = 0;
        /*for(Integer i=0; i<familyProgramList.size();i++){
            for (Product2 prod : metadat.prodsByPlatform.get(familyProgramList[i]).products) {
                system.assertEquals(listProducts[counter].Id,prod.Id,'Different product id was expected.');
                system.assertEquals(listProducts[counter].Family,familyProgramList[i],'Different product family was expected.');
            }
            counter++;
        }*/
                
        system.debug(LoggingLevel.ERROR, 'SOQL query usage at the end of ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage at the end of ' + unitTestName + ' unit test: ' + Limits.getDMLStatements()); 
    }//testPositiveGetMetadata
    
    
    /** 
   	*  This test method contains tests for validating the behavior of getMetadata method
    *  SCENARIO:
    *      1. Call getMetadata method
    *  	   2. Assert core columns are returned
    *      3. Assert products are correctly returned and grouped by platform
    *      4. Assert product with New Family is returned
    */
    static testMethod void testPositiveGetMetadataProductWithNewFamily() {
        //Create test data
      	createTestData();
        Opportunity currentOpp = listOpps[0];

        String unitTestName = 'testPositiveGetMetadataProductWithNewFamily';
        
        /*Create Products*/
        listProducts = V2SA_Util_TestDataFactory.generateObjectList('Product2', 2, null, false);
    
        for (Integer i = 0;i < listProducts.size();i++){
            listProducts[i].Family = 'New Family';            
            listProducts[i].IsActive = true;
            listProducts[i].V2SACL1__Month_Issue_Close_Date__c = date.today();
        }     
        insert listProducts;
        
        listStandardPricebookEntries = new List<PricebookEntry>();
        /*Add Products to Standard Pricebook*/
        for(Product2 objProduct : listProducts) {
            listStandardPricebookEntries.add(new PricebookEntry(Pricebook2Id = standardpricebookId, 
                                                                Product2Id = objProduct.Id, UnitPrice = 25000, 
                                                                isActive = true)); 
           
                
        }
        insert listStandardPricebookEntries;  
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before start test in ' + unitTestName + 
                     ' unit test: ' + Limits.getDMLStatements());
        system.Test.StartTest();
          
       	V2_ProdSel.Metadata metadat = V2_ProdSel.getMetadata(currentOpp.Id);
        
        system.debug(LoggingLevel.ERROR, 'SOQL query usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage before stop test in ' + unitTestName + ' unit test: ' + Limits.getDMLStatements());
        system.Test.StopTest();

        //ASSERTS
        
        //Assert custom metadata types are returned
        System.Assert(metadat.fields.coreColumns.size() > 0);
        

        system.debug(LoggingLevel.ERROR, 'SOQL query usage at the end of ' + unitTestName + ' unit test: ' + Limits.getQueries());
        system.debug(LoggingLevel.ERROR, 'DML Statements usage at the end of ' + unitTestName + ' unit test: ' + Limits.getDMLStatements()); 
    }//testPositiveGetMetadataProductWithNewFamily
}