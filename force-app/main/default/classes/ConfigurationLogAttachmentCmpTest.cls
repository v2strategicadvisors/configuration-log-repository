@isTest
public class ConfigurationLogAttachmentCmpTest {
    @testSetup
    private static void setup(){
        //Create a Configuraiton Log
        Configuration_Log__c configLog = new Configuration_Log__c();
        configLog.Name = 'Test Configuration Log';
        insert configLog;
        
		//Create common test Attachments
		List<Attachment> testAttachments = new List<Attachment>();
        for(Integer i=0; i < 3; i++){
            Attachment att = new Attachment();
            att.Name = 'Test ' + i + '.xml';
            att.Body = Blob.valueOf('');
            att.ParentId = configLog.Id;
            testAttachments.add(att);
        }        
        insert testAttachments;
    }
    
    @isTest
    private static void whenGettingAttachments(){
         // Perform test
        Test.startTest();
        	Configuration_Log__c configLog = [SELECT Id, Name FROM Configuration_Log__c limit 1];
        	List<Attachment> attachments = ConfigurationLogAttachmentsCmp.getConfigLogAttachments(configLog.Id);
        Test.stopTest();
        
        System.assertEquals(3, attachments.size());
    }
    
    @isTest
    private static void whenCreatingMergePackages(){
        // Perform test
        Test.startTest();
        	List<Attachment> attachments = [SELECT Id, Name, Body, ParentId FROM Attachment];
        	ConfigurationLogAttachmentsCmp.mergeSelectedAttachments(attachments);
        Test.stopTest();
        
        System.assertEquals(0, 0);
    }
}