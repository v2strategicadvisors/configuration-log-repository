/**
 * This class is the Common Test Utility Class dedicated for Test Data Creation in test classes.
 * The main goal is to make test classes easier to manage and easy to read by abstracting out superfluous code.
 * @author V2 Strategic Advisors - Maxwell Tatem
 * Created: Mar 23, 2018
 
 * Default values for object:       User, Account, Contact, Opportunity (without Primary Contact), Product2 (without Brand), PriceBook2, Meeting Summary
 *
 * Usage:                           CREATE 1 OBJECT
 *                                              a) Create map with custom field values if needed.                                                       Example: Map<String, Object> AccountCustomFieldValues = new Map<String, Object>{'Name'=>'Custom Test Name'};
 *                                              b) If you would like to have unique name use sufix (flag) __unique with field name e.g. Name__unique    Example: Map<String, Object> AccountCustomFieldValues = new Map<String, Object>{'Name__unique'=>'Custom Test Name'};                                
 *                                              c) To create object with default values use:                                                            Example: Account obj = (Account)V2SA_Util_TestDataFactory.generateObject('Account', null, false/true); Where arguments are (String objectType, String objRecordType, Boolean doInsert)                               
 *                                              d) To create object with custom field values add additional values parameter:                           Example: Account obj = (Account)V2SA_Util_TestDataFactory.generateObject('Account', null, false/true, AccountCustomFieldValues); Where arguments are (String objectType, String objRecordType, Boolean doInsert, Map<String, Object> values)                             
 *                                  CREATE LIST OF OBJECTS          ...
 *                                              a) Create map with custom field values if needed.
 *                                              b) If you would like to have unique name use sufix (flag) __unique with field name e.g. Name__unique
 *                                              c) To create list object with default values use:                                                       Example: list<Account> obj = (list<Account>)V2SA_Util_TestDataFactory.generateObjectList('Account', 10, 'Agency', false/true); Where arguments are (String objectType, Integer numOfRecords, String objRecordType, Boolean doInsert)
 *                                              d) To create list object with custom field values add additional values parameter:                      Example: list<Account> obj = (list<Account>)V2SA_Util_TestDataFactory.generateObjectList('Account', 10, 'Agency', false/true, AccountCustomFieldValues); Where arguments are (String objectType, Integer numOfRecords, String objRecordType, Boolean doInsert, Map<String, Object> values)
 *																																						Example for with additinal custom values for the record:  Map<String, Object> AccountCustomFieldValues = new Map<String, Object>{'Name'=>'New Different Name', 'Description' => 'New value, not defoult', 'Industry' => 'New field populated'};		
 */
@isTest(seeAllData = false)
public without sharing class V2SA_Util_TestDataFactory {
	
	// cache for defaults values
    static Map<String, Map<String, Object>> mapMapObjectDefaultsByObjectName;
    // cache for unique fields for current execution of creating records
    static set<String> uniqueFields;
    // cache for unique fields for current execution of creating records
    static Integer globalIterator;
    //static Id recordtypeIdAdvertiserBrand = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Advertiser / Brand').getRecordTypeId();
    
    //-------------------------------------------------------------------------------------------------------------------------------//
    //--------------------------------------------------Default object field values--------------------------------------------------//
    //-------------------------------------------------------------------------------------------------------------------------------//
    
    // User Object default field values
    final static Map<String, Object> UserDefaults = new Map<String, Object>{
        'LastName__unique'          => 'LastName', //
        'Alias'                     => 'xy86ec', //should be unique
        'Email'                     => 'LastName@example.com', //should be unique
        'Username'                  => 'LastName@example.com', //should be unique
        'CommunityNickname__unique' => 'xy86ec',
        'IsActive'                  => true,
        'TimeZoneSidKey'            => 'America/New_York',
        'LocaleSidKey'              => 'en_US',
        'EmailEncodingKey'          => 'ISO-8859-1',
        'LanguageLocaleKey'         => 'en_US',
        'Department'                => 'Corporate Sales'        
    };
    
    // Account Object default field values (for Orgs WITHOUT State/Country Picklist enabled)
    final static Map<String, Object> AccountDefaults = new Map<String, Object>{
	        'Name__unique'          => 'Account Name',
            'BillingCountry' 		=> 'United States',
            //'BillingCountryCode' 	=> 'US',
            'BillingState' 			=> 'New York',
            //'BillingStateCode' 		=> 'NY',
            'ShippingCountry'		=> 'United States',
            //'ShippingCountryCode'	=> 'US',
            'ShippingState' 		=> 'New York',
            'BillingPostalCode' 	=> '10001',
            'Description'           => 'Test account'//,
        	//'RecordTypeId'              => recordtypeIdAdvertiserBrand // Add Id recordtype for Advertiser / Brand
    };
        
    // Account Object default field values (for Orgs WITH State/Country Picklist enabled)
    // NOTE: when State/Country Picklists are enabled, ensure that the base fields
    // (such as Billing/Shipping Country/State) are set to the standard 'long format'
    // value (United States) and the Code fields (Billing/Shipping Country/State Code)                           
    // are set to the ISO 2-char value (US, NY)
    final static Map<String, Object> AccountDefaultsCountryPicklist = new Map<String, Object>{
	        'Name__unique'          => 'Account Name',
            'BillingCountry' 		=> 'United States',
            'BillingCountryCode' 	=> 'US',
            'BillingState' 			=> 'New York',
            'BillingStateCode' 		=> 'NY',
            'BillingCity'			=> 'New York City',
            'BillingStreet'			=> 'Broadway',
            'ShippingCountry'		=> 'United States',
            'ShippingCountryCode'	=> 'US',
            'ShippingState' 		=> 'New York',
            'BillingPostalCode' 	=> '10001',
            'Description'           => 'Test account'//,
        	//'RecordTypeId'              => recordtypeIdAdvertiserBrand // Add Id recordtype for Advertiser / Brand
    };
    
    // Contact Object default field values
    final static Map<String, Object> ContactDefaults = new Map<String, Object>{
        'FirstName__unique'                     => 'FirstName',         
        'LastName__unique'                      => 'LastName',
        'Phone'                                 => '1112224455'           
    };
    
    
    // Opportunity Object default field values
    final static Map<String, Object> OpportunityDefaults = new Map<String, Object>{
        'Name__unique'              		=> 'Test Opportunity',
        'StageName'                 		=> 'Prospecting',
        'Probability'               		=> 20,
        'Amount'             				=> 1000,
        'CloseDate'                		 	=> Date.today(),
        'NextStep'                 		 	=> 'TestNextStep',
        //'Group__c'							=> 'Lifestyle',
        'Type'  							=> 'Local'
    };
    
    // Product Object default field values
    final static Map<String, Object> Product2Defaults = new Map<String, Object>{
        'Name__unique'              => 'Test Product',
        'Family'                    => 'Internet Ad',
        'IsActive'                  => true
        //'Market__c'					=> 'Seatle',
        //'Market_ID__c'				=> '40'
    };
    
    // PriceBook Object default field values
    final static Map<String, Object> Pricebook2Defaults = new Map<String, Object>{
        'Name'              => 'Price Book',
        'IsActive'                  => true
    };
    
    // Meeting Summary Object default field values
    final static Map<String, Object> MeetingSummaryDefaults = new Map<String, Object>{
        'Name__unique'				=> 'Meeting Summary Name',
        'Purpose__c'				=> 'Entertaining',
        'Status__c'					=> 'Planned',
        //'Meeting_Location__c'       => 'V2 Internal Event',
        'Meeting_Date__c'			=> Date.today()
    };

    // Account Assignment Object default field values
    final static Map<String, Object> AccountAssignmentDefaults = new Map<String, Object>{
        'Status__c'					=> 'Active'
    };
    
    // Brand Object default field values
    final static Map<String, Object> Brand2Defaults = new Map<String, Object>{
        'Name__unique'              => 'Test Brand',
       // 'Group__c'                  => 'Active Interest Network',
        'Status__c'                 => 'Active'
    };
    
    final static Map<String, Object> OpportunityLineItemDefaults = new Map<String, Object>{
        'ab2__ABSTartDate__c'               => system.today(),
        'ab2__abEndDate__c'                 => system.today(),
        'UnitPrice'  						=> 2000,
      //  'single_ad_cost__c'               => 1000,
        'P_O_Number__c'						=> 'ChC51000',
        'Quantity' 							=> 1,
        'Ad_Section__c' 					=> 'TEST',
        'Bill_To__c' 						=> 'Account'
    };
     final static Map<String, Object> PricebookEntryDefaults = new Map<String, Object>{
        'UnitPrice'               				=> 1
     };
         
     final static Map<String, Object> TransactionYearDefaults = new Map<String, Object>{
        'Name'               				=> String.valueof(Date.today().year()),
        'Feature_Start_Date__c'				=> Date.today(),
        'Start_Date__c'						=> Date.today()
     };
	
     final static Map<String, Object> QuotaDefaults = new Map<String, Object>{
        'Family__c' 						=> 'BOL',
		'Period_End__c' 					=> Date.Today().addDays(7),
		'Period_Start__c' 					=> Date.Today(),
        'Date__c'							=> Date.Today(),
        'Amount__c'							=> 8009
     };
     
     final static Map<String, Object> OpportunityLineItemScheduleDefaults = new Map<String, Object>{
        'Revenue' 							=> 1000,
        'Type'								=> 'Revenue',
		'ScheduleDate' 						=> Date.Today().addDays(7)
     };
     
     final static Map<String, Object> SalesTeamSplitDefaults = new Map<String, Object>{
     };
     
     final static Map<String, Object> ScenariosDefaults = new Map<String, Object>{
             'Is_Active__c'					=> true,
             'Contract_Market_Percent__c'	=> 75,
             'National_Print_Percent__c'	=> 75
     };
     final static Map<String, Object> AccountHistoryDefaults = new Map<String, Object>{
         
     };
    /**
    *   @description    Method to fill up map mapMapObjectDefaultsByObjectName with default values for particular sObjects  
    */
    private static void fillMapOfDefaultsForObjects() {
        mapMapObjectDefaultsByObjectName = new Map<String, Map<String, Object>>();
        mapMapObjectDefaultsByObjectName.put('User',        UserDefaults);
        mapMapObjectDefaultsByObjectName.put('Contact',     ContactDefaults);
        mapMapObjectDefaultsByObjectName.put('Opportunity', OpportunityDefaults);
        mapMapObjectDefaultsByObjectName.put('Product2',    Product2Defaults);
        mapMapObjectDefaultsByObjectName.put('Pricebook2',  Pricebook2Defaults);
        mapMapObjectDefaultsByObjectName.put('Meeting_Summary__c',	MeetingSummaryDefaults);
        mapMapObjectDefaultsByObjectName.put('Account_Split__c',	AccountAssignmentDefaults);
        mapMapObjectDefaultsByObjectName.put('Brand__c',	Brand2Defaults);
        mapMapObjectDefaultsByObjectName.put('OpportunityLineItem',	OpportunityLineItemDefaults);
        mapMapObjectDefaultsByObjectName.put('PricebookEntry',	PricebookEntryDefaults);
        mapMapObjectDefaultsByObjectName.put('TransactionYear__c',	TransactionYearDefaults);
        mapMapObjectDefaultsByObjectName.put('Quota__c',	QuotaDefaults);
        mapMapObjectDefaultsByObjectName.put('OpportunityLineItemSchedule',	OpportunityLineItemScheduleDefaults);
        mapMapObjectDefaultsByObjectName.put('Sales_Team_Split__c',	SalesTeamSplitDefaults);
        mapMapObjectDefaultsByObjectName.put('Scenarios__c', ScenariosDefaults);
        mapMapObjectDefaultsByObjectName.put('AccountHistory', AccountHistoryDefaults);
        
        // Check if State/Country Picklist feature is enabled in the Org and use the
        // appropriate fieldMap for Account
        String fieldName = 'Countrycode';
        SObjectType sot = Schema.getGlobalDescribe().get('User');
        Boolean isPicklistEnabled = sot.getDescribe().fields.getMap().keySet().contains(fieldName.toLowerCase());
        if(isPicklistEnabled){
	        mapMapObjectDefaultsByObjectName.put('Account',     AccountDefaultsCountryPicklist);            
        }else{
            mapMapObjectDefaultsByObjectName.put('Account',     AccountDefaults);            
        }
    }
	
	/**
	 * @description A method to get a random number.
	 *
	 * @param range		for number (0 - range)
	 * @return			random number
	 */
	public static Integer randomNumber(Integer range) {
		Double x = math.random() * range;
		return x.intValue();
	}
    
    /**
    *   @description    Helper method to generate Market Split object record  with default field values.
    *
    *   @param doInsert         Boolean value: If it is set to TRUE, record is inserted to database
    *   @return                 Newly generated Market Split
    */ 
    
	public static Opportunity generateOpportunityObject(Boolean doInsert){
        Opportunity OpportunityObject = new Opportunity();
        
        if(mapMapObjectDefaultsByObjectName == null){
            fillMapOfDefaultsForObjects();
        }
         Map<String, Object> defaults = mapMapObjectDefaultsByObjectName.get('Opportunity');
                       
        OpportunityObject.Name = (String) defaults.get('Name__unique');
        OpportunityObject.StageName = (String) defaults.get('StageName');
        OpportunityObject.Probability = (Decimal) defaults.get('Probability');
        OpportunityObject.Amount = (Decimal) defaults.get('Amount');
        OpportunityObject.CloseDate = (Date) defaults.get('CloseDate');
        OpportunityObject.NextStep = (String) defaults.get('NextStep');
        OpportunityObject.Type = (String) defaults.get('Type');
        
        
        return OpportunityObject;
    }
    
    public static string generateUniqueId(){
        List<string> characters = new List<string>{'a','b','c','d','e','f','g','h', 'i','j','k','l','m','n','o','p','q','r','s','t','u','v','w', 'x','y','z','1','2','3','4','5','6','7','8','9','0' };

        string randomText = '';
        
        for(Integer i=0;i < 15;i++){
            string randomChars = characters[randomNumber(characters.size())];
            
            randomText += randomChars;
            
        }

        return randomText;
 
    }
    
    /**
    *   @description    Helper method to generate User object record  with default field values.
    *
    *   @param userProfile      Name of the profile which will be set for the User
    *   @param isActive         Boolean value to set User as Active or Inactive
    *   @param doInsert         Boolean value: If it is set to TRUE, record is inserted to database (Accounts are inserted by defualt, because we need them for connect with Opportunity)
    *   @return                 Newly generated User
    */ 
    public static User generateUserObject(String userProfile, Boolean isActive, Boolean doInsert){
                User userObject                     =   new User();
                userObject.FirstName            =   userProfile.deleteWhitespace() + 'FirstName';
                userObject.LastName             =   userProfile.deleteWhitespace() + 'LastName';
                userObject.Alias                =   'xy86ec';
                userObject.Email                =   userProfile.deleteWhitespace()+'@example.com';//To Do generate random address
               // userObject.Username             =   userProfile.deleteWhitespace()+'@example.com.'+String.valueOf(randomNumber(5000));
                userObject.Username             =  	generateUniqueId()+'@example.com';
        		userObject.CommunityNickname    =   generateUniqueId();
                //userObject.UserRoleId         =   Role.id;
                userObject.IsActive             =   isActive;
                userObject.TimeZoneSidKey       =   'America/New_York';
                userObject.LocaleSidKey         =   'en_US';
                userObject.EmailEncodingKey     =   'ISO-8859-1';
                userObject.ProfileId            =   getUserProfileId(userProfile);
                userObject.LanguageLocaleKey    =   'en_US';
                userObject.Department           =   'Corporate Sales';
        //to do additional params i.e. additional field values: map of fields with values, key fieldapiname value will be value
      
        if(doInsert) insert userObject;
        
        return userObject;
    }//generateUserObject(userProfile, isActive, doInsert)
    
    /**
    *   @description    Helper method to generate User List i.e. user records with default field values.
    *   @param numUsers         Number of newly generated Users
    *   @param userProfile      Name of the profile which will be set for Users
    *   @param isActive         Boolean value to set User as Active or Inactive
    *   @param doInsert         Boolean value: If it is set to TRUE, record is inserted to database (Accounts are inserted by defualt, because we need them for connect with Opportunity)
    *   @return                 List of newly generated users
    */ 
    public static list<User> generateUserList(Integer numUsers, String userProfile, Boolean isActive, Boolean doInsert){
                list<User> userList             = new list<User>();
        Id profileId = getUserProfileId(userProfile);
        // generate User Object 
        for(Integer i=0;i<numUsers;i++) {
                User userObject                 =   new User();
                userObject.LastName             =   'T3stLastName' + userProfile +String.ValueOf(i);
                userObject.Alias                =   'xy'+ String.ValueOf(i);
                userObject.Email                =   userProfile.deleteWhitespace()+String.ValueOf(i)+'@example.com';
                userObject.Username             =   userProfile.deleteWhitespace()+String.ValueOf(i)+'@example.com';
                userObject.CommunityNickname    =   'xy'+ String.ValueOf(i);
                //userObject.UserRoleId         =   Role.id;
                userObject.IsActive             =   isActive;
                userObject.TimeZoneSidKey       =   'America/New_York';
                userObject.LocaleSidKey         =   'en_US';
                userObject.EmailEncodingKey     =   'ISO-8859-1';
                userObject.ProfileId            =   profileId;
                userObject.LanguageLocaleKey    =   'en_US';
                userObject.Department           =   'Corporate Sales';
            userList.add(userObject);
        }
        
        if(doInsert) insert userList;
        
        return userList;
    }//generateUserList(numUsers, userProfile, isActive, doInsert)
    
    //--------------------------------------------------------------------------------------------//
    //--------------------------------- Generate sObject record ---------------------------------//
    // Helper method to generate commonly used sObjects record with default field values.
    /**
    *   @description    Helper method to generate commonly used sObject record with default field values.
    *   
    *   @param objectType       API Name (String) of the object eg. Account, Contact, Opportunity
    *   @param objRecordType    (can be null) Record type name (String) for newly generated object
    *   @param doInsert         Boolean value: If it is set to TRUE, the record is inserted into the database
    *   @return                 New object instance
    */ 
    public static sObject generateObject(String objectType, String objRecordType, Boolean doInsert){
        list<sObject> listObjects = generateObjectList(objectType, 1, objRecordType, doInsert, null);
        return listObjects[0];
    }
    
    // Helper method to generate commonly used sObjects record with default and additional/custom field values
    /**
    *   @description    Helper method to generate commonly used sObject record with default and additional/special field values.
    *   
    *   @param objectType       API Name (String) of the object eg. Account, Contact, Opportunity
    *   @param objRecordType    (can be null) Record type name (String) for newly generated object
    *   @param doInsert         Boolean value: If it is set to TRUE, the record is inserted into the database
    *   @param values           Map of custom (additional/custom) field values (Map<String, Object>)
    *   @return                 New object instance
    */ 
    public static sObject generateObject(String objectType, String objRecordType, Boolean doInsert, Map<String, Object> values){
        list<sObject> listObjects = generateObjectList(objectType, 1, objRecordType, doInsert, values);
        return listObjects[0];
    }
    
    //-----------------------------------------------------------------------------------------------------//
    //--------------------------------- Generate list of sObject records ---------------------------------//
    // Helper method to generate list of commonly used sObjects record with default field values
    /**
    *   @description    Helper method to generate list of commonly used sObject records with default field values.
    *   
    *   @param objectType       API Name (String) of the object eg. Account, Contact, Opportunity
    *   @param numOfRecords     Number (Integer) of new records  to generate
    *   @param objRecordType    (can be null) Record type name for newly generated object
    *   @param doInsert         Boolean value: If it is set to TRUE, the record is inserted into the database
    *   @return                 New list of genterated object instances
    */ 
    public static list<sObject> generateObjectList(String objectType, Integer numOfRecords, String objRecordType, Boolean doInsert){
        return generateObjectList(objectType, numOfRecords, objRecordType, doInsert, null);
    }//generateAccountList
    
    
    // Helper method to generate list of commonly used sObjects record with default and additional/custom field values
    /**
    *   @description    Helper method to generate list of commonly used sObject records with default and additional/special field values.
    *   
    *   @param objectType       API Name (String) of the object eg. Account, Contact, Opportunity
    *   @param numOfRecords     Number (Integer) of new records  to generate
    *   @param objRecordType    (can be null) Record type name for newly generated object
    *   @param doInsert         Boolean value: If it is set to TRUE, the record is inserted into the database
    *   @param values           Map fo custom (additional/custom) field values (Map<String, Object>)
    *   @return                 New list of genterated object instances
    */ 
    public static list<sObject> generateObjectList(String objectType, Integer numOfRecords, String objRecordType, Boolean doInsert, Map<String, Object> values){
        // clear uniqueFields from previous run
        uniqueFields = new Set<String>();
        // iterate global iterator so we will have unique values for unique fields
        if(globalIterator == null) {
            globalIterator = 0;
        } else { 
            globalIterator++;
        }
        
        // Final list of (created) objects.
        list<sObject> listObjects = new list<sObject>();
        // Create instance of the new object.
        sObject newObj;
        try{
            newObj = Schema.getGlobalDescribe().get(objectType).newSObject();
        } catch(Exception ex) {
            throw new V2SA_Util_TestDataFactoryException('Incorrect object type:' + objectType);
        }
        
        // If map of default values is not set, populate it.
        if(mapMapObjectDefaultsByObjectName == null) {
            fillMapOfDefaultsForObjects();
        }
        
        // get default values for object
        Map<String, Object> defaults = mapMapObjectDefaultsByObjectName.get(objectType);
        // if there is no defaults for objectType
        if(defaults == null) {
            defaults = new Map<String, Object>();
        }
        
        // put default field values to new object 
        addFieldValues(objectType, newObj, defaults);
        
        // If there is record type, populate it. 
        // When objRecordType is not defined (it is null) salesforce will create record with default RecordType (based on actual running user profile)
        if(objRecordType != null && (Object)getObjectRecordTypeId(objectType, objRecordType) != null){
        	newObj.put('RecordTypeId', (Object)getObjectRecordTypeId(objectType, objRecordType));
        }
        
        // put additional/special field values
        if(values != null)  { 
            addFieldValues(objectType, newObj, values); 
        }

        Schema.DescribeSObjectResult sObjectResult = Schema.getGlobalDescribe().get(objectType).getDescribe();
        Schema.DescribeFieldResult sObjectFieldResult;
        // Clone the object the number of times requested
        for(Integer i=0; i<numOfRecords; i++) {
            sObject clonedSObj = newObj.clone(false, true);
            
            // when there are some unique fields, code will set other idetifier to make them unique
            for(String field : uniqueFields) {
                sObjectFieldResult = sObjectResult.fields.getMap().get(field).getDescribe();
                if (sObjectFieldResult.getType() == Schema.DisplayType.Integer) {
                    clonedSObj.put(field, Integer.valueOf(clonedSObj.get(field)) + i);
                }
                else if (sObjectFieldResult.getType() == Schema.DisplayType.Double) {
                    clonedSObj.put(field, Double.valueOf(clonedSObj.get(field)) + i);
                } else {
                    clonedSObj.put(field, clonedSObj.get(field) + '--' + String.valueOf(i));
                }
            }
            listObjects.add(clonedSObj);
        }
        
        // if it's requested, insert object into database   
        if(doInsert) {
            insert listObjects; 
        }
        
        // clear uniqueFields, becuase this method can be executed for different object type later
        uniqueFields = null;
        
        return listObjects;
    }//generateObjectList(String objectType, Integer numOfRecords, String objRecordType, Boolean doInsert, Map<String, Object> values)
    
    /**
    *   @description    Helper method to populate object's fields with (entered) before defined values and use global iterator if the field is unique (so that we can work with unique value/record when the record is generated in test class more than once).
    *
    *   @param objectType   Name of the sObject Type    
    *   @param sObj         Instance of the sObject
    *   @param values       Map of Object values by field Name
    */
    private static void addFieldValues(String objectType, sObject sObj, Map<String, Object> values) {
        // Loop through the map of fields and if they are null on the object, fill them.

        Schema.DescribeSObjectResult sObjectResult = Schema.getGlobalDescribe().get(objectType).getDescribe();
        Schema.DescribeFieldResult sObjectFieldResult;

        for (String field : values.keySet()) {
            String objField = field;
            
            // unique flag: remove it from name and mark this field as unique 
            Boolean uniqueField = false;
            if(field.contains('__unique')) {
                objField = objfield.remove('__unique');
                uniqueField = true;
            }
            
            // put value for field (in objField should be salesforce field without flags)
            sObj.put(objField, values.get(field));
            
            // logic for unique flag: add field to uniqueFields for know which fields are unique when we will clone record
            if(uniqueField){
                sObjectFieldResult = sObjectResult.fields.getMap().get(objField).getDescribe();
                uniqueFields.add(objField);
                // globalIterator is for one set of the records the same. Other idetifier will be added during cloning when create more records is needed
                if (sObjectFieldResult.getType() == Schema.DisplayType.Integer) {
                    sObj.put(objField, Integer.valueOf(sObj.get(objField)) + globalIterator);
                }
                else if (sObjectFieldResult.getType() == Schema.DisplayType.Double) {
                    sObj.put(objField, Double.valueOf(sObj.get(objField)) + globalIterator);
                } else {
                    sObj.put(objField, sObj.get(objField) + '--' + String.valueof(globalIterator));
                }
            }
            
        }
    }
    
    // ------------------------------- Helper methods to create particular object records e.g. Accounts with Opporunities. Opportunity is created without Primary Contact. ---------------------------------------//
    /**
    *   @description Helper method to create/generate Accounts with Opportunity records with default field values.
    *   
    *   @param numAccts             Number of Accounts which we want to generate
    *   @param numOpps              Number of Opportunities which we want to generate for each Account
    *   @param acctRecordType       (can be null) Record type name for newly generated Accounts
    *   @param oppRecordType        (can be null) Record type name for newly generated Opportunities
    *   @param doInsert             Boolean value: If it is set to TRUE, the record is inserted into database (Accounts are inserted by defualt, because we need them for connect with Opportunity)
    *   @return oppList             List of new Opportunities
    */ 
    public static list<Opportunity> createAcctsWithOppsList(Integer numAccts, Integer numOpps, String acctRecordType, String oppRecordType, Boolean doInsert) {
        Map<String, object> customValues = new Map<String, object> {'Start_Date__c' => date.today().addDays(10)};
        return createAcctsWithOppsList(numAccts, numOpps, acctRecordType, oppRecordType, doInsert, customValues);        
    }//createAcctsWithOppsList(numAccts, numOpps, acctRecordType, oppRecordType, doInsert)
    /**
    *   @description    Helper method to create/generate Accounts with Opportunity records custom field values.
    *
    *   @param numAccts             Number of Accounts which we want to generate
    *   @param numOpps              Number of Opportunities which we want to generate for each Account
    *   @param acctRecordType       (can be null) Record type name for newly generated Opportunities
    *   @param oppRecordType        (can be null) Record type name for newly generated Accounts
    *   @param doInsert             Boolean value: If it is set to TRUE, record is inserted to database (Accounts are inserted by defualt, because we need them for connect with Opportunity)
    *   @param oppValues            Map of additional/custom field values for Opportunities
    *   @return                     List of new Opportunities
    */ 
    public static list<Opportunity> createAcctsWithOppsList(Integer numAccts, Integer numOpps, String acctRecordType, String oppRecordType, Boolean doInsert, Map<String, Object> oppValues) {
        list<Account> listAccts = generateObjectList('Account', numAccts, acctRecordType, true); // must be true, because we need insert accounts
        
        return createOppsListForAccts(listAccts, numOpps, oppRecordType, doInsert, oppValues);//listOpps;
    }//createAcctsWithOppsList(numAccts, numOpps, acctRecordType, oppRecordType, doInsert, Map<String, Object> oppValues)

    /**
    *   @description    Helper method to Opportunity records for given Accounts. With default Opportunity field values.
    *
    *   @param myAcctList           List of existing Accounts   
    *   @param numOpps              Number of Opportunities which we want to generate for each Account
    *   @param oppRecordType        (can be null) Record type name for newly generated Accounts
    *   @param doInsert             Boolean value: If it is set to TRUE, record is inserted to database (Accounts are inserted by defualt, because we need them for connect with Opportunity)
    *   @return                     List of new Opportunities
    */ 
    public static list<Opportunity> createOppsListForAccts(list<Account> myAcctList, Integer numOpps, String oppRecordType, Boolean doInsert) {
        return createOppsListForAccts( myAcctList, numOpps, oppRecordType, doInsert, null);
    }//createOppsListForAccts
    
    /**
    *   @description    Helper method to create/generate Opportunity records for given Accounts.
    *
    *   @param myAcctList           List of existing Accounts   
    *   @param numOpps              Number of Opportunities which we want to generate for each Account
    *   @param oppRecordType        (can be null) Record type name for newly generated Opportunities
    *   @param doInsert             Boolean value: If it is set to TRUE, record is inserted to database (Accounts are inserted by defualt, because we need them for connect with Opportunity)
    *   @param oppValues            Map of additional/custom field values for Opportunities
    *   @return                     List of new Opportunities
    */ 
    public static list<Opportunity> createOppsListForAccts(list<Account> myAcctList, Integer numOpps, String oppRecordType, Boolean doInsert,  Map<String, Object> oppValues) {
        list<Opportunity> listOpp = new list<Opportunity>();
        for (Integer j=0;j<myAcctList.size();j++) {
            Account acct = myAcctList[j];            
            // For each account add opportunities
            for (Integer k=numOpps*j;k<numOpps*(j+1);k++) {
                Map<String, Object> OpportunityCustomFieldValues = new Map<String, Object>{'AccountId' => acct.Id};
                if(oppValues != null){
                    OpportunityCustomFieldValues.putAll(oppValues);
                }
                Opportunity objOpp = (Opportunity)generateObject('Opportunity', oppRecordType, false, OpportunityCustomFieldValues);
                listOpp.add(objOpp);
            }
        }

        if(doInsert) insert listOpp; //if doInsert = true: do insert opportunities // Insert all opportunities for all accounts
            
        return listOpp;
    }//createOppsListForAccts(myAcctList, numOpps, oppRecordType, doInsert, additionalValues)
    
    /**
    *   @description    Helper method to get user profile Id based on Profile Name.
    *
    *   @param profileName      Profile Name
    *   @return                 Profile Id
    */ 
    public static Id getUserProfileId(String profileName){
        Profile userProfile = [Select p.Id From Profile p where Name = :profileName limit 1][0];
        return userProfile.Id;
    }//getUserProfileId(profileName)
    
    /**
    *   @description    Helper method to get sObject record type Id by Record Type name. If record type is empty or null select default record type for the user.
    *   
    *   @param objectType           API Name of the object eg. Account
    *   @param recordTypeName       Record type Name 
    *   @return                     Record type Id
    */ 
    public static Id getObjectRecordTypeId(String objectType, String recordTypeName ){
        Id sObjectRecordTypeId;
        
        if(recordTypeName == null || recordTypeName == '') {
            recordTypeName = getDefaultObjectRecordTypeName(objectType);
        }
        
        try{
            sObjectRecordTypeId = Schema.getGlobalDescribe().get(objectType).getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        } catch(Exception ex) {
            throw new V2SA_Util_TestDataFactoryException('Incorrect object type: '+objectType + ' or record type name: ' + recordTypeName);
        }
            
        return sObjectRecordTypeId;
    }

    
  
    /**
    *   @descriptionHelper method to get default account record type Name for actual running user.
    *   
    *   @param objectType       API name of the object. eg. Account
    *   @return                 Returns default Record Type Name for actual running user.
    */
    static String getDefaultObjectRecordTypeName(String objectType){
        Schema.DescribeSObjectResult describeObj;
        
        //find all Object's record types
        try{
            describeObj = Schema.getGlobalDescribe().get(ObjectType).getDescribe();    
        } catch(Exception ex) {
            throw new V2SA_Util_TestDataFactoryException('Incorrect object type: '+objectType);
        }
        
        List<Schema.RecordTypeInfo> rtInfos = describeObj.getRecordTypeInfos();
        String defaultRecordTypeName;
        //check each Record Type. If one of them is default for actual running user, return its name.
        //If there in no (custom-additional) Record Type this method will return defaultRecordTypeName = 'Master'
        for (Schema.RecordTypeInfo info : rtInfos) {
          if (info.isDefaultRecordTypeMapping()) {
            defaultRecordTypeName = info.getName();
            break;
          }
        }
        return defaultRecordTypeName;
    }//getDefaultObjectRecordTypeName  
    
    /**
    * @description Custom exception class
    */
    public class V2SA_Util_TestDataFactoryException extends Exception {}
    
}