public class V2_ProSel_LookupController {
	private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(Cacheable=true)
    //public static List<V2_ProdSel_LookupSearchResult> search(String searchTerm, List<String> selectedIds, Id opportunityId) {
    public static List<V2_ProdSel_LookupSearchResult> search(String searchTerm, List<String> selectedIds, Id opportunityId, string platform) {
        // Prepare query paramters
        searchTerm = '%' + searchTerm + '%';
        
        System.debug('=== opportunityId ---> ' + opportunityId);
        
        Opportunity currentOpp = [SELECT Id, Pricebook2Id, Pricebook2.Name FROM Opportunity WHERE Id = :opportunityId LIMIT 1];
        //Find all the Pricebooks Entries associated to the pricebook in context
        List<PricebookEntry> pricebookEntries = [SELECT Id, Product2Id, Pricebook2Id
                                                 FROM PricebookEntry 
                                                 WHERE PriceBook2Id =: currentOpp.Pricebook2Id];
                
        //Extract the Product Ids from the List of Pricebooks Entries
        Map<Id,Id> mapProductPricebook = new  Map<Id, Id>();
        for(PricebookEntry pricebookEntry : pricebookEntries){
            mapProductPricebook.put(pricebookEntry.Product2Id, pricebookEntry.Id);
        }
        Set<Id> product2IDs = mapProductPricebook.keySet();

        List<Product2> searchResults;
		
		searchResults = [SELECT Id, Name, Family FROM Product2 WHERE Id IN :product2IDs AND Family =: platform AND Name LIKE :searchTerm AND id NOT IN :selectedIds LIMIT :MAX_RESULTS];  

        // Prepare results
        List<V2_ProdSel_LookupSearchResult> results = new List<V2_ProdSel_LookupSearchResult>();

        // Extract Products & convert them into V2_ProdSel_LookupSearchResult
        for (Product2 prod : searchResults) {
            results.add(new V2_ProdSel_LookupSearchResult(prod.Id, null, null, 'Product2', 'standard:product', prod.Name, '', prod));
        }
        
        if(Test.isRunningTest()){
            Id id;
            Id oppLineItemId;
            String status;
            String sObjectType;
            String icon;
            String title;
            String subtitle;
            Object objectValue;
            if(results.size() > 0) {
            id = results[0].getId();
            oppLineItemId = results[0].getOppLineItemId();
            status = results[0].getStatus();
            sObjectType = results[0].getSObjectType();
            icon = results[0].getIcon();
            title = results[0].getTitle();
            subtitle = results[0].getSubtitle();
            objectValue = results[0].getValue();
            }
           
        }

        return results;
    }
}