@isTest
public class V2_AccEstimatesManager_Test {
    @testSetup
    private static void setup(){
        List<Account_Filter__c> accountFilters = new List<Account_Filter__c>();
        for(Integer i = 0; i < 2; i++){
            Account_Filter__c af = new Account_Filter__c();
            af.Name = 'testing '+i;
            accountFilters.add(af);
        }
        
        insert accountFilters;
    }
    /** 
    *  This test method contains tests for validating the behavior of the getChannelPicklistValues method
    *  SCENARIO:
    *      1. Call getChannelPicklistValues method
    *      2. Assert at least one channel was returned
    */
    static testMethod void testGetChannelPicklistValues() { 
        system.Test.StartTest();
        
        List<string> channels = V2_AccEstimatesManager.getChannelPicklistValues();
		
        system.Test.StopTest();
           
        system.assert(channels.size()>0, ''); 
    }//testGetChannelPicklistValues
    
    
    /** 
    *  This test method contains tests for validating the behavior of the getFilterOptions method
    *  SCENARIO:
    *      1. Insert test account filters data
    * 	   2. Call getFilterOptions method
    *      3. Assert filters are returned
    */
    static testMethod void testGetFilterOptions() {
        List<Account_Filter__c> accFilters = new List<Account_Filter__c>();
        
        system.Test.StartTest();
        
        V2_AccEstimatesManager.FilterOption filterOpt = V2_AccEstimatesManager.getFilterOptions();
		
        system.Test.StopTest();
           
        //Includes My Estimated Accounts default Option (added through code)
        system.assertEquals(3, filterOpt.listLocalSelect.size(), 'Should have returned 3'); 
    }//testGetFilterOptions
    
    /** 
    *  This test method contains tests for validating the behavior of the getFilterOptions method
    *  SCENARIO:
    *      1. Call getFilterOptions method
    *      2. Assert no filters are returned (other than My Estimated Accounts)
    */
    static testMethod void testNegativeGetFilterOptions() {
        system.Test.StartTest();
        
        V2_AccEstimatesManager.FilterOption filterOpt = V2_AccEstimatesManager.getFilterOptions();
		
        system.Test.StopTest();
           
        //Assert My Estimated Accounts option exists
        system.assertEquals(filterOpt.listLocalSelect.size(), 3); 
    }//testNegativeGetFilterOptions
    
    
    /** 
    *  This test method contains tests for validating the behavior of the getEstimateSummaryData method
    *  SCENARIO:
    *      1. Call getEstimateSummaryData method
    *      2. Assert only one item in the listAccWithSnapwrapper of the summary is returned (since there is only one account)
    */
    static testMethod void testGetEstimateSummaryData() {
        Account testAcc = new Account(name = 'test account');
        insert testAcc;
               
        system.Test.StartTest();
                
       	V2_AccEstimatesManager.EstimateSummaryData summary = V2_AccEstimatesManager.getEstimateSummaryData(System.Today().year(), 1, '','',0,5);
        V2_AccEstimatesManager.EstimateSummaryData summary1 = V2_AccEstimatesManager.getEstimateSummaryData(null, 1, '','',0,5);    
        system.Test.StopTest(); 
            
        system.assertEquals(1, summary.listAccWithSnapwrapper.size(), 
                            'Summary did not return the right amount of accounts.');
        System.assertEquals(0, summary1.listAccWithSnapwrapper.size(), 
                           'Summary did not return the right amount of accounts 0');
    }//testGetEstimateSummaryData
    
    /** 
    *  This test method contains tests for validating the behavior of the process of filtering accounts by name 
    *  from the getEstimateSummaryData method
    *  SCENARIO:
    *      1. Call getEstimateSummaryData method with an account name
    *      2. Assert only the account searched for is returned
    */
    static testMethod void testGetEstimateSummaryDataFilterByAccName() {
        Account testAcc = new Account(name = 'test account');
        insert testAcc;
        
        testAcc = new Account(name = 'second account');
        insert testAcc;
       
        system.Test.StartTest();
        
       	V2_AccEstimatesManager.EstimateSummaryData summary = V2_AccEstimatesManager.getEstimateSummaryData(System.Today().year(), 1, '', 'test', 0, 5);
        
        system.Test.StopTest();
 
        system.assertEquals(1, summary.listAccWithSnapwrapper.size(), 
                            'Summary did not return the right amount of accounts.'); 
        system.assertEquals('test account', summary.listAccWithSnapwrapper[0].accountName, 
                            'Summary did not return the right account.'); 
    }//testGetEstimateSummaryDataFilterByAccName
    
    /** 
    * This test method contains tests for validating the behavior of the process of filtering accounts 
    * using an account filter in the getEstimateSummaryData method
    * SCENARIO:
    *      1. Call getEstimateSummaryData method with an account filter with Industry and name filled out
    *      2. Assert only the account searched for is returned
    */
    static testMethod void testGetEstimateSummaryDataFilterThruAccFilter() {
        Account testAcc = new Account(name = 'test account', Industry = 'Automotive');
        insert testAcc;
        
        Account testAcc2 = new Account(name = 'second account', Industry = 'Agency');
        insert testAcc2;
        
        
        Account_filter__c accFilter = new Account_filter__c();
        accFilter.name = 'acFilter';
        accFilter.Account_Name_Contains__c = 'second account';
        accFilter.Account_Industry__c = 'Agency';
        accFilter.My_Estimated_Accounts__c = false;
        insert accFilter;
               
        system.Test.StartTest();
                
       	V2_AccEstimatesManager.EstimateSummaryData summary = 
            V2_AccEstimatesManager.getEstimateSummaryData(System.Today().year(), 1, accFilter.Id, '', 0, 5);
              
        system.Test.StopTest();
            
        system.assertEquals(1, summary.listAccWithSnapwrapper.size(), 
                            'Summary did not return the right amount of accounts.'); 
        system.assertEquals('second account', summary.listAccWithSnapwrapper[0].accountName, 
                            'Summary did not return the right account.'); 
    }//testGetEstimateSummaryDataFilterThruAccFilter
    
    /** 
    * This test method validates an empty summary is returned if filter does not return any accounts
    * SCENARIO:
    *      1. Call getEstimateSummaryData method with an account name
    *      2. Assert no account is returned
    */
    static testMethod void testGetEstimateSummaryEmptyIfNoAccounts() {
    	system.Test.StartTest();
        
       	V2_AccEstimatesManager.EstimateSummaryData summary = V2_AccEstimatesManager.getEstimateSummaryData(System.Today().year(), 1, '', '', 0, 5);
      
        system.Test.StopTest();
            
        system.assertEquals(0, summary.listAccWithSnapwrapper.size(), 
                            'Summary did not return the right amount of accounts.');
    }//testGetEstimateSummaryEmptyIfNoAccounts
        
    /** 
    * This test method contains tests for validating the behavior of the process of filtering accounts 
    * using the My Estimated Accounts filter
    * SCENARIO:
    *      1. Call getEstimateSummaryData method with the My Estimated Accounts filter
    * 	   2. Assert two accounts are returned (the one where the current user is sales rep 1 and New business)
    *      3. Assert the first account is the one where the current user is sales rep 1
    * 	   4. Assert the second account returned is New Business
    * 	   5. Assert the year from the account estimate snapshot is returned correctly
    * 	   6. Assert the Quarterly Account Estimate has the right estimate
    */
    static testMethod void testGetEstimateSummaryDataFilterThruMyAccounts() {
        List<string> channels = V2_AccEstimatesManager.getChannelPicklistValues();
		
        User u = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, true);
        User u2 = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, true);
          
        String currYear = String.valueOf(System.Today().year());
        system.runAs(u)
        {
            Account testAcc = new Account(name = 'test account', Industry = 'Automotive');
            insert testAcc;
            
            Account testAcc2 = new Account(name = 'second account', Industry = 'Agency');
            insert testAcc2;
            
            Account_Estimate_Snapshot__c ags = new Account_Estimate_Snapshot__c();
            ags.Advertiser__c = testAcc.Id;
            ags.sales_rep_1__c = u.Id;
            ags.year__c = currYear;
            insert ags;
            
            Quarterly_Account_Estimate__c qag = new Quarterly_Account_Estimate__c();
            qag.quarterly_Estimate__c = 1000;
            qag.account_estimate_snapshot__c = ags.Id;   
            qag.Channel__c = channels[0];
            qag.quarter__c = 'Q1';
         
            insert qag;
            
            qag = new Quarterly_Account_Estimate__c();
            qag.quarterly_Estimate__c = 1000;
            qag.account_estimate_snapshot__c = ags.Id;   
            qag.Channel__c = channels[1];
            qag.quarter__c = 'Q1';
         
            insert qag;
            
            Account_Estimate_Snapshot__c ags2 = new Account_Estimate_Snapshot__c();
            ags2.Advertiser__c = testAcc2.Id;
            ags2.sales_rep_1__c = u2.Id;
            ags2.year__c = currYear;
            insert ags2;
            
            Quarterly_Account_Estimate__c qag2 = new Quarterly_Account_Estimate__c();
            qag2.quarterly_Estimate__c = 1000;
            qag2.account_estimate_snapshot__c = ags2.Id;
            insert qag2;
            
            system.Test.StartTest();
                        
            V2_AccEstimatesManager.EstimateSummaryData summary = 
                V2_AccEstimatesManager.getEstimateSummaryData(Integer.valueOf(currYear), 1, 'My Estimated Accounts', '', 0, 5);
                      
            system.Test.StopTest();
                
            system.assertEquals(2, summary.listAccWithSnapwrapper.size(), 
                                'Summary did not return the right amount of accounts.'); 
            system.assertEquals('test account', summary.listAccWithSnapwrapper[0].accountName, 
                                'Summary did not return the right account.'); 
            system.assertEquals('New Business', summary.listAccWithSnapwrapper[1].accountName, 
                                'Summary did not return the right account.'); 
            system.assertEquals(String.valueOf(currYear), summary.listAccWithSnapwrapper[0].listPlanSnapshots[0].BFS.year__c, 
                                'Account Estimate Snapshot does not have the right year.');
            system.assertEquals(1000, summary.listAccWithSnapwrapper[0].listPlanSnapshots[0].listChannQFs[0].selYearQF.quarterly_Estimate__c, 
                                'Quarterly Account Estimate does not have the right estimate.');            
        }
    }//testGetEstimateSummaryDataFilterThruMyAccounts
    
    
    /** 
    * This test method contains tests for validating the getEstimateSummaryData method
    * when the quarter passed is different to one 
    * SCENARIO:
    *      1. Create an account with three quarterly account estimates with quarters 1, 2 and 3
    *  	   2. Call getEstimateSummaryData method with an empty filter and a quarter of 2
    *      3. Assert the quarterly account estimates returned are the ones for the quarter 2 and 3
    */
    static testMethod void testGetEstimateSummaryWithTwoYearsReturned() {
        List<string> channels = V2_AccEstimatesManager.getChannelPicklistValues();
		
        User u = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, true);
    
        Account testAcc = new Account(name = 'test account', Industry = 'Automotive');
        insert testAcc;
        
        Account_Estimate_Snapshot__c ags = new Account_Estimate_Snapshot__c();
        ags.Advertiser__c = testAcc.Id;
        ags.sales_rep_1__c = u.Id;
        ags.year__c = '2020';
        insert ags;
        
        Quarterly_Account_Estimate__c qag = new Quarterly_Account_Estimate__c();
        qag.quarterly_Estimate__c = 1000;
        qag.account_estimate_snapshot__c = ags.Id;   
        qag.Channel__c = channels[0];
        qag.quarter__c = 'Q1';
        
        insert qag;
        
		qag.Id = null;
        qag.quarter__c = 'Q2';
        
        insert qag;
        
        qag.Id = null;
        qag.quarter__c = 'Q3';
        
        insert qag;
            
        system.Test.StartTest();
        	V2_AccEstimatesManager.EstimateSummaryData summary = V2_AccEstimatesManager.getEstimateSummaryData(2020, 2, '', '', 0, 5);
        system.Test.StopTest();
        
        system.assertEquals(1, summary.listAccWithSnapwrapper.size(), 
                            'Summary did not return the right amount of accounts.'); 
        system.assertEquals('test account', summary.listAccWithSnapwrapper[0].accountName, 
                            'Summary did not return the right account.'); 
        system.assertEquals('2020', summary.listAccWithSnapwrapper[0].listPlanSnapshots[0].BFS.year__c, 
                            'Account Estimate Snapshot does not have the right year.');
        system.assertEquals('Q2', summary.listAccWithSnapwrapper[0].listPlanSnapshots[0].listChannQFs[0].selYearQF.quarter__c, 
                            'Quarterly Account Estimate does not have the right quarter.');
        system.assertEquals('Q3', summary.listAccWithSnapwrapper[0].listPlanSnapshots[1].listChannQFs[0].selYearQF.quarter__c, 
                            'Quarterly Account Estimate does not have the right quarter.');
        
    }//testGetEstimateSummaryWithTwoYearsReturned
    
    /**
    * This test method contains tests for validating the behavior of the process of filtering accounts 
    * using the Snapshot Owner filter and Snapshot Owner Office in an account filter
    * SCENARIO:
    *      1. Create two test users
    *      2. Create an account filter with snapshot year as 2020 and My Estimated Accounts as true 
           3. Execute run as to run as first user
    *      4. Create two test accounts
    *      5. Create two account estimate snapshots, one per each account (first one with first user as sales rep and second one tied to second user) 
                and three quarterly account estimates, two for the first snapshot and one for the other
    *      6. Assert only the first account is returned since it is tied to the running user through a snapshot and that a New Business
            account is also returned due to the My Estimated Accounts checkbox being true
    *      7. Assert right values are returned and that year is 2020 as set in the account filter
    */
    static testMethod void testGetEstimateSummaryDataFilterThruMyAccountsandAGSYear() {
        List<string> channels = V2_AccEstimatesManager.getChannelPicklistValues();
		
        User u = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, true);
        User u2 = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, true);
          
        Account_filter__c accFilter = new Account_filter__c();
        accFilter.AES_Year__c = '2020';
        accFilter.name = 'acFilter';
        accFilter.My_Estimated_Accounts__c  = true;
        insert accFilter;
        
        system.runAs(u)
        {

            Account testAcc = new Account(name = 'test account', Industry = 'Automotive');
            insert testAcc;
            
            Account testAcc2 = new Account(name = 'second account', Industry = 'Agency');
            insert testAcc2;
            
            Account_Estimate_Snapshot__c ags = new Account_Estimate_Snapshot__c();
            ags.Advertiser__c = testAcc.Id;
            ags.sales_rep_1__c = u.Id;
            ags.year__c = '2020';
            insert ags;
            
            Quarterly_Account_Estimate__c qag = new Quarterly_Account_Estimate__c();
            qag.quarterly_Estimate__c = 1000;
            qag.account_estimate_snapshot__c = ags.Id;   
            qag.Channel__c = channels[0];
            qag.quarter__c = 'Q1';
         
            insert qag;
            
            qag = new Quarterly_Account_Estimate__c();
            qag.quarterly_Estimate__c = 1000;
            qag.account_estimate_snapshot__c = ags.Id;   
            qag.Channel__c = channels[1];
            qag.quarter__c = 'Q1';
         
            insert qag;
            
            Account_Estimate_Snapshot__c ags2 = new Account_Estimate_Snapshot__c();
            ags2.Advertiser__c = testAcc2.Id;
            ags2.sales_rep_1__c = u2.Id;
            ags2.year__c = '2020';
            insert ags2;
            
            Quarterly_Account_Estimate__c qag2 = new Quarterly_Account_Estimate__c();
            qag2.quarterly_Estimate__c = 1000;
            qag2.account_estimate_snapshot__c = ags2.Id;
            insert qag2;
            
            system.Test.StartTest();
                        
            V2_AccEstimatesManager.EstimateSummaryData summary = V2_AccEstimatesManager.getEstimateSummaryData(2020, 1, accFilter.Id, '', 0, 5);
                      
            system.Test.StopTest();
                
            system.assertEquals(2, summary.listAccWithSnapwrapper.size(), 
                                'Summary did not return the right amount of accounts.'); 
            system.assertEquals('test account', summary.listAccWithSnapwrapper[0].accountName, 
                                'Summary did not return the right account.'); 
            system.assertEquals('New Business', summary.listAccWithSnapwrapper[1].accountName, 
                                'Summary did not return the right account.'); 
            system.assertEquals('2020', summary.listAccWithSnapwrapper[0].listPlanSnapshots[0].BFS.year__c, 
                                'Account Estimate Snapshot does not have the right year.');
            system.assertEquals(1000, summary.listAccWithSnapwrapper[0].listPlanSnapshots[0].listChannQFs[0].selYearQF.quarterly_Estimate__c, 
                                'Quarterly Account Estimate does not have the right estimate.');   
        }
    }//testGetEstimateSummaryDataFilterThruMyAccountsandAGSYear

    /**
    * This test method contains tests for validating the behavior of the process of filtering accounts 
    * using the Snapshot Owner filter in an account filter
    * SCENARIO:
    *      1. Create two test users
    *      2. Create an account filter with snapshot owner as John Samuels (the first user)
           3. Execute run as to run as first user
    *      4. Create two test accounts
    *      5. Create two account estimate snapshots, one per each account (first one with first user as sales rep and second one tied to second user) 
                and three quarterly account estimates, two for the first snapshot and one for the other
    *      6. Assert only the first account is returned since it belongs to the snapshot owner specified in the account filter
    *      7. Assert right values are returned
    */
    static testMethod void testGetEstimateSummaryDataFilterThruSnapOwner() {
        List<string> channels = V2_AccEstimatesManager.getChannelPicklistValues();
		
        User u = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, false);
        u.FirstName = 'John';
        u.LastName = 'Samuels';
        insert u;
        
        User u2 = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, true);
          
        Account_filter__c accFilter = new Account_filter__c();
        accFilter.Snapshot_Owner__c = 'John Samuels';
        accFilter.name = 'acFilter';
        insert accFilter;
        
        system.runAs(u)
        {
            Account testAcc = new Account(name = 'test account', Industry = 'Automotive');
            insert testAcc;
            
            Account testAcc2 = new Account(name = 'second account', Industry = 'Agency');
            insert testAcc2;
            
            Account_Estimate_Snapshot__c ags = new Account_Estimate_Snapshot__c();
            ags.Advertiser__c = testAcc.Id;
            ags.sales_rep_1__c = u.Id;
            ags.year__c = '2020';
            insert ags;
            
            Quarterly_Account_Estimate__c qag = new Quarterly_Account_Estimate__c();
            qag.quarterly_Estimate__c = 1000;
            qag.account_estimate_snapshot__c = ags.Id;   
            qag.Channel__c = channels[0];
            qag.quarter__c = 'Q1';
         
            insert qag;
            
            qag = new Quarterly_Account_Estimate__c();
            qag.quarterly_Estimate__c = 1000;
            qag.account_estimate_snapshot__c = ags.Id;   
            qag.Channel__c = channels[1];
            qag.quarter__c = 'Q1';
         
            insert qag;
            
            Account_Estimate_Snapshot__c ags2 = new Account_Estimate_Snapshot__c();
            ags2.Advertiser__c = testAcc2.Id;
            ags2.sales_rep_1__c = u2.Id;
            ags2.year__c = '2020';
            insert ags2;
            
            Quarterly_Account_Estimate__c qag2 = new Quarterly_Account_Estimate__c();
            qag2.quarterly_Estimate__c = 1000;
            qag2.account_estimate_snapshot__c = ags2.Id;
            insert qag2;
            
            system.Test.StartTest();
                        
            V2_AccEstimatesManager.EstimateSummaryData summary = V2_AccEstimatesManager.getEstimateSummaryData(2020, 1, accFilter.Id, '', 0, 5);
                      
            system.Test.StopTest();
                
            system.assertEquals(1, summary.listAccWithSnapwrapper.size(), 
                                'Summary did not return the right amount of accounts.'); 
            system.assertEquals('test account', summary.listAccWithSnapwrapper[0].accountName, 
                                'Summary did not return the right account.'); 
            system.assertEquals('2020', summary.listAccWithSnapwrapper[0].listPlanSnapshots[0].BFS.year__c, 
                                'Account Estimate Snapshot does not have the right year.');
            system.assertEquals(1000, summary.listAccWithSnapwrapper[0].listPlanSnapshots[0].listChannQFs[0].selYearQF.quarterly_Estimate__c, 
                                'Quarterly Account Estimate does not have the right estimate.');   
        }
    }//testGetEstimateSummaryDataFilterThruSnapOwner

    /**
    * This test method contains tests for validating the behavior of the process of filtering accounts 
    * using the My Accounts filter in an account filter and the AGS year
    * SCENARIO:
    *      1. Create 3 accounts, two of them with the type "Partner" and different names, and one with the type "Other"
    * 	   2. Call getEstimateSummaryData method with an account filter with the type "Partner" and 
    * 		  the account name contains equal to "account"
    * 	   3. Assert only one account is returned
    *      3. Assert the account is the one matching both filters
    */ 
    static testMethod void testGetEstimateSummaryDataFilterThruAccFilterTypeAndName() {
        
        Account testAcc = new Account(name = 'account test', Type = 'Partner');
        insert testAcc;
        
        Account testAcc2 = new Account(name = 'other example', Type = 'Partner');
        insert testAcc2;
        
        Account testAcc3 = new Account(name = 'example 3', Type = 'Other');
        insert testAcc3;
        
        Account_filter__c accFilter = new Account_filter__c();
        accFilter.Account_Name_Contains__c = 'account';
        accFilter.Account_Type__c = 'Partner';
        accFilter.My_Estimated_Accounts__c = false;
        insert accFilter;
               
        system.Test.StartTest();
        
       	V2_AccEstimatesManager.EstimateSummaryData summary = 
            V2_AccEstimatesManager.getEstimateSummaryData(System.Today().year(), 1, accFilter.Id, '', 0, 5);
        
        system.Test.StopTest();
            
        system.assertEquals(1, summary.listAccWithSnapwrapper.size(), 
                            'Summary did not return the right amount of accounts.'); 
        system.assertEquals('account test', summary.listAccWithSnapwrapper[0].accountName, 
                            'Summary did not return the right account.'); 
    }//testGetEstimateSummaryDataFilterThruAccFilterTypeAndName
    
    /** 
    * This test method contains tests for validating the behavior of the process of saving an estimate
    * SCENARIO:
    *      1. Call save method
    *      2. Assert the estimate inserted is correct
    *      3. Assert a snapshot is created with the running user as sales rep 1 and with the year passed to the estimate
    */
    static testMethod void testSaveEstimate() {
        User u = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, true);
        
        system.runAs(u)
        {
            Account testAcc = new Account(name = 'test account', Industry = 'Automotive');
            insert testAcc;
            
            List<String> channelsList = V2_AccEstimatesManager.getChannelPicklistValues();
                   
            system.Test.StartTest();
            
            Quarterly_Account_Estimate__c toSave = new Quarterly_Account_Estimate__c();
            toSave.quarterly_Estimate__c = 500;
            
            Quarterly_Account_Estimate__c estimate = V2_AccEstimatesManager.save(JSON.serialize(toSave), 
                                                '2021', channelsList[0], testAcc.Id);
          	Quarterly_Account_Estimate__c estimate1 = V2_AccEstimatesManager.save(JSON.serialize(toSave), 
                                                '2021', channelsList[0], null);
            
            system.Test.StopTest();
                
            system.assertEquals(500, estimate.quarterly_Estimate__c, 
                                'Estimate does not have right amount.'); 
            
            Account_Estimate_Snapshot__c snap = [select id, year__c, sales_rep_1__c from 
                                                 Account_Estimate_Snapshot__c where Id = :estimate.Account_Estimate_Snapshot__c];
            system.assertEquals('2021', snap.year__c, 
                                'Year of snapshot was not set correctly.');
            system.assertEquals(u.Id, snap.sales_rep_1__c, 
                                'Sales Rep of snapshot was not set correctly.');
        }
    }//testSaveEstimate  
    
    /** 
    * This test method contains tests for validating the behavior of the process of saving an estimate
    * when an account estimate snapshot already exists
    * SCENARIO:
    *      1. Call save method
    *      2. Assert the estimate inserted is correct
    *      3. Assert snapshot associated was the one first created
    * 	   4. Assert no new snapshot is created
    */
    static testMethod void testSaveEstimateWithExistAGS() {
        User u = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, true);
        User u2 = V2SA_Util_TestDataFactory.generateUserObject('System Administrator', true, true);
        
        system.runAs(u2)
        {
            Account testAcc = new Account(name = 'test account', Industry = 'Automotive');
			insert testAcc;
            
            List<String> channelsList = V2_AccEstimatesManager.getChannelPicklistValues();
            
            system.Test.StartTest();
            
            Account_Estimate_Snapshot__c ags = new Account_Estimate_Snapshot__c();
            ags.Advertiser__c = testAcc.Id;
            ags.sales_rep_1__c = u2.Id;
            ags.year__c = '2020';
            insert ags;
            
            List<Account_Estimate_Snapshot__c> agsList = [select id, year__c from Account_Estimate_Snapshot__c];
            system.assert(agsList.size() == 1);
            
            Quarterly_Account_Estimate__c toSave = new Quarterly_Account_Estimate__c();
            toSave.quarterly_Estimate__c = 500;
            
            Quarterly_Account_Estimate__c estimate = V2_AccEstimatesManager.save(JSON.serialize(toSave), 
                                                                         '2020', channelsList[0], testAcc.Id);
            
            
            system.Test.StopTest();
            
            system.assertEquals(500, estimate.quarterly_Estimate__c, 
                                'Estimate does not have right amount.'); 
            
            Account_Estimate_Snapshot__c snap = [select id, year__c from Account_Estimate_Snapshot__c where Id = :estimate.Account_Estimate_Snapshot__c];
            system.assertEquals('2020', snap.year__c, 
                                'Year of snapshot was not set correctly.'); 
            agsList = [select id, year__c from Account_Estimate_Snapshot__c];
            system.assert(agsList.size() == 1);
        }
    }//testSaveEstimateWithExistAGS
    
    /** 
    * This test method asserts quarterly account estimate is not saved if quarterly estimate field is null
    * SCENARIO:
    *      1. Call save method with a QAG with null quarterly estimate
    * 	   2. Assert QAG was not saved
    *      3. Call save method again with a set quarterly estimate
    * 	   4. Assert QAG was saved
    */
    static testMethod void testEstimateNotSavedIfQuarterlyEstimateIsNull() {
        Account testAcc = new Account(name = 'test account', Industry = 'Automotive');
        insert testAcc;
        
        List<String> channelsList = V2_AccEstimatesManager.getChannelPicklistValues();
        
        system.Test.StartTest();
            Quarterly_Account_Estimate__c toSave = new Quarterly_Account_Estimate__c();
            toSave.quarterly_Estimate__c = null;
            
            Quarterly_Account_Estimate__c estimate = V2_AccEstimatesManager.save(JSON.serialize(toSave), 
                                                                         '2020', channelsList[0], testAcc.Id);
            system.assertEquals(null, estimate.quarterly_Estimate__c, 
                            'Estimate should have been null.'); 
        
            List<Quarterly_Account_Estimate__c> estimatesList = [select id from Quarterly_Account_Estimate__c];
            
            system.assertEquals(0, estimatesList.size(), 
                                'Quarterly account estimate was saved even though it should not have.');
            
        	toSave.quarterly_Estimate__c = 500;
         	estimate = V2_AccEstimatesManager.save(JSON.serialize(toSave), 
                                               '2020', channelsList[0], testAcc.Id);
        
            system.assertEquals(500, estimate.quarterly_Estimate__c, 
                            'Estimate does not have the right amount.'); 
        
        	estimatesList = [select id from Quarterly_Account_Estimate__c];
            
            system.assertEquals(1, estimatesList.size(), 
                                'Quarterly account estimate was not saved even though it should have.');
            
        system.Test.StopTest();
        
    }//testEstimateNotSavedIfQuarterlyEstimateIsNull
    
    /**
    * This test method validates the behavior of the process of
    * getting default settings from the Quarterly Estimate Setting
    * SCENARIO:
    *      1. Insert sample custom setting 
    * 	   2. Call getDefaultSettings method
    *      2. Assert year, red limit and yellow limit passed to custom setting are the same returned by method
    */
    static testMethod void testGetDefaultSettings() {
        Quarterly_Estimate_Setting__c setting = new Quarterly_Estimate_Setting__c ();
        setting.Base_Year__c = '2022';
        setting.Red_Limit__c = 0;
        setting.Yellow_Limit__c = 20;
        setting.Name = 'name';
        insert setting;
        
        system.Test.StartTest();
        	Quarterly_Estimate_Setting__c settings = V2_AccEstimatesManager.getDefaultSettings();
         	system.assertEquals(settings.Base_Year__c, '2022', 
                            'Year returned is different than year set.');
            system.assertEquals(settings.Red_Limit__c, 0, 
                            'Red Limit is different than limit set.');
            system.assertEquals(settings.Yellow_Limit__c, 20, 
                            'Yellow Limit is different than limit set.');
        system.Test.StopTest();
    }//testGetBaseYear
    
    /**
    * This test method validates the behavior of the process of
    * getting default settings from the Quarterly Estimate Setting when the Base Year is set incorrectly
    * SCENARIO:
    *      1. Insert sample custom setting with a string instead of a number for the year
    * 	   2. Call getDefaultSettings method
    *      2. Assert exception message is "Check Base Year Setting and make sure it is set to a proper year."
    */
    static testMethod void testGetDefaultSettingsWithError() {
        Quarterly_Estimate_Setting__c setting = new Quarterly_Estimate_Setting__c();
        setting.Base_Year__c = 'lkkl';
        setting.Red_Limit__c = 0;
        setting.Yellow_Limit__c = 20;
        setting.Name = 'name';
        insert setting;
        
        system.Test.StartTest();
            try{
                Quarterly_Estimate_Setting__c settings = V2_AccEstimatesManager.getDefaultSettings();
            }catch(V2_AccEstimatesManager.ImproperYearException iye){
                System.Assert(iye.getMessage().
                              contains('Check Base Year Setting and make sure it is set to a proper year.'));
            }
        system.Test.StopTest();
    }//testGetBaseYearWithError
}