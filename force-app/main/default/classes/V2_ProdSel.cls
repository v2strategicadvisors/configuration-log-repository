public class V2_ProdSel {
    
    public class customTypesException extends Exception {}
    private static Set<string> lookupTypeList;
    private static Set<string> activePlatfromList;
    private static Set<string> requireMultiSelect;
    
   	public class LocalSelect {
     	@AuraEnabled
        public string value {get; set;}
        @AuraEnabled
        public string label {get; set;}
        
        public LocalSelect(string value, string label){           
            this.value = value;
            this.label = label;
        }
    }
    
    public class FieldsMetadata {
        @AuraEnabled public List<V2SACL1__Prod_Sel_Core_Cols__mdt> coreColumns;
        @AuraEnabled public List<V2SACL1__Platform_Entry_Type__mdt> platformEntryTypes;
                
        public FieldsMetadata(){
            this.coreColumns = new List<V2SACL1__Prod_Sel_Core_Cols__mdt>();
            this.platformEntryTypes = new List<V2SACL1__Platform_Entry_Type__mdt>();
        }
    }
    
    public class OLIsByPlatform {
        @AuraEnabled public String platform;
        @AuraEnabled public List<OpportunityLineItem> OLIs;
                
        public OLIsByPlatform(String platform){
            this.platform = platform;
            this.OLIs = new List<OpportunityLineItem>();
        }
    }
       
    public class ProductsByPlatform {
        @AuraEnabled public String platform;
        @AuraEnabled public List<Product2> products;
        @AuraEnabled public Boolean isProgram;
        @AuraEnabled public Boolean requiredMultiSelect;
                
        public ProductsByPlatform(String platform){
            this.platform = platform;
            this.products = new List<Product2>();
            this.isProgram = lookupTypeList.contains(platform);
            this.requiredMultiSelect = requireMultiSelect.contains(platform);
        }
    }    
    
    public class Metadata {
        @AuraEnabled public FieldsMetadata fields;
        @AuraEnabled public Map<String, ProductsByPlatform> prodsByPlatform;
    }
    
    private static String constructOLIsQuery(){
        FieldsMetadata fieldsMeta = getFieldsMetadata();
        //Get all fields from platform entry type and save those that start with Col_ in a set
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().
            get('V2SACL1__Platform_Entry_Type__mdt').getDescribe().fields.getMap();
		
        Set<String> colFields = new Set<String>();
        for(String field : objectFields.keySet()) {
            if(field.startsWith('Col_')){
                colFields.add(field);
            }
        }

        System.debug('==== colFields ---> ' + colFields);
        
    	Set<String> olisFieldsToQuery = new Set<String>();

        //Go through the records of the prod core cols metadata type
        //and save the api name values in the olisFieldsToQuery set
        for(V2SACL1__Prod_Sel_Core_Cols__mdt coreCol : fieldsMeta.coreColumns){
			olisFieldsToQuery.add(coreCol.V2SACL1__ApiName__c);       
        }
        
        String oliQuery = 'SELECT Product2.Family, Product2.V2SACL1__Month_Issue_Close_Date__c, V2SACL1__BrandVoice_Program__c, V2SACL1__Sub_Category__c, ';
        
        for(String sfield : olisFieldsToQuery) {
            oliQuery += sfield + ',';
        }  
        oliQuery = oliQuery.removeEnd(',');
        
        oliQuery += ' FROM OpportunityLineItem WHERE OpportunityId =: opportunityId and '+
           '(Product2.V2SACL1__Month_Issue_Close_Date__c = LAST_YEAR or ' +
           'Product2.V2SACL1__Month_Issue_Close_Date__c = THIS_YEAR or ' +
           'Product2.V2SACL1__Month_Issue_Close_Date__c = NEXT_YEAR) ORDER BY Product2.V2SACL1__Month_Issue_Close_Date__c';

        system.debug('LR:' + oliQuery);

        return oliQuery;
    }
    
    /**
    * @description This method returns the existing OLIs grouped by platform
    * @param opportunityId refers to Id of opportunity being worked with
    */
    @AuraEnabled
    public static List<OLIsByPlatform> getExistingOLIsByPlatform(Id opportunityId){
        
        //verifyPricebook(opportunityId);
        
        List<OLIsByPlatform> platformOLIs = new List<OLIsByPlatform>();
        
        List<OpportunityLineItem> listOLIS = new List<OpportunityLineItem>();
		
		String oliQuery = constructOLIsQuery();        
        try {
            listOLIS = Database.query(oliQuery);
        } catch(QueryException e) {
            System.debug('=== QueryException ---> ' + e);
            throw new customTypesException('Contact your administrator, something is wrong with the custom metadata types configuration');
        }
                       
        String currProdFam = '';
        OLIsByPlatform platOLI = new OLIsByPlatform(currProdFam);
        for(OpportunityLineItem oli : listOLIS){             
            if(oli.Product2.Family != currProdFam){
                currProdFam = oli.Product2.Family;
                platOLI = new OLIsByPlatform(currProdFam);                
                platformOLIs.add(platOLI);
            }
            
            platOLI.OLIs.add(oli);
        }
        system.debug('olis: ' + platformOLIs);
        return platformOLIs;  
    }
    
    private static FieldsMetadata getFieldsMetadata(){
		List<V2SACL1__Platform_Entry_Type__mdt> platEntryTypes;

        platEntryTypes = [SELECT Id, V2SACL1__Platform_Name__c, V2SACL1__Entry_Type__c, V2SACL1__Is_Active__c, V2SACL1__Multi_Select_Picklit__c FROM V2SACL1__Platform_Entry_Type__mdt WHERE V2SACL1__Is_Active__c = true];
   
        lookupTypeList = new Set<String>();
        activePlatfromList = new Set<String>();
        requireMultiSelect = new Set<String>();

        for (V2SACL1__Platform_Entry_Type__mdt pe : platEntryTypes) {
            activePlatfromList.add(pe.V2SACL1__Platform_Name__c);

            if (pe.V2SACL1__Entry_Type__c == 'Lookup') {
                lookupTypeList.add(pe.V2SACL1__Platform_Name__c);
            }
            if (pe.V2SACL1__Multi_Select_Picklit__c) {
                requireMultiSelect.add(pe.V2SACL1__Platform_Name__c);
            }
        }

        FieldsMetadata fieldsMeta = new FieldsMetadata();
        fieldsMeta.platformEntryTypes = platEntryTypes;
        
        List<V2SACL1__Prod_Sel_Core_Cols__mdt> coreColumns = [SELECT MasterLabel, V2SACL1__ApiName__c, V2SACL1__Is_Active__c, V2SACL1__Type__c,
                                                     V2SACL1__Order__c, V2SACL1__ColumnWidth__c FROM V2SACL1__Prod_Sel_Core_Cols__mdt WHERE V2SACL1__Is_Active__c = true ORDER BY V2SACL1__Order__c ASC];
        
        fieldsMeta.coreColumns = coreColumns;
        
        return fieldsMeta;
    }


    private static Map<String, ProductsByPlatform> getProductsListByPlatform(Id opportunityId){
        
        Opportunity currentOpp = [SELECT Id, Pricebook2Id, Pricebook2.Name FROM Opportunity WHERE Id = :opportunityId LIMIT 1];
        //Find all the Pricebooks Entries associated to the pricebook in context
        List<PricebookEntry> pricebookEntries = [SELECT Id, Product2Id, Pricebook2Id
                                                 FROM PricebookEntry 
                                                 WHERE PriceBook2Id =: currentOpp.Pricebook2Id];
                
        //Extract the Product Ids from the List of Pricebooks Entries
        Map<Id,Id> mapProductPricebook = new  Map<Id, Id>();
        for(PricebookEntry pricebookEntry : pricebookEntries){
            mapProductPricebook.put(pricebookEntry.Product2Id, pricebookEntry.Id);
        }
        
        Set<Id> product2IDs = mapProductPricebook.keySet();
        
        List<Product2> products = [SELECT Id, Name, V2SACL1__Month_Issue_Close_Date__c, Family, V2SACL1__Sub_Category__c FROM Product2 WHERE Id IN: product2IDs AND Family IN: activePlatfromList AND IsActive = true 
                                   AND (V2SACL1__Month_Issue_Close_Date__c = LAST_YEAR or 
                                   V2SACL1__Month_Issue_Close_Date__c = THIS_YEAR or
                                   V2SACL1__Month_Issue_Close_Date__c = NEXT_YEAR) ORDER BY V2SACL1__Month_Issue_Close_Date__c ASC];

        System.debug('**** products ---> ' + products);
        
        
        /*===================================================================================*/
        Map<String, ProductsByPlatform> mapPlatforms = new Map<String, ProductsByPlatform>();
        for(Product2 prod : products){
            string family = prod.Family;
            if(mapPlatforms.containsKey(family) && !lookupTypeList.contains(family)){
                mapPlatforms.get(prod.Family).products.add(prod);
            } else {
                ProductsByPlatform prodPlat = new ProductsByPlatform(family);
                if(!lookupTypeList.contains(family)){
                    prodPlat.products.add(prod);
                }
                mapPlatforms.put(family, prodPlat);
            }
        }
             
        //return platformProducts;
        return mapPlatforms;
    }
    /**
    * @description This method returns the metadata needed in product selector
    * Including columns to show and a list of products grouped by platform
    */
    @AuraEnabled
    public static Metadata getMetadata(Id opportunityId ){        
        Metadata meta = new Metadata();
              
        meta.fields = getFieldsMetadata();
        meta.prodsByPlatform = getProductsListByPlatform(opportunityId);
        
        return meta;
    }
    
    /**
    * @description This method is used to save changes to OLIs
    */
    @AuraEnabled
    public static void saveOpportunityLineItems(String OLIsToUpsertJson){        
        List<OpportunityLineItem> OLIsToUpsertList = OLIsToUpsertJson != '' ? 
            (List<OpportunityLineItem>)System.JSON.deserializeStrict(OLIsToUpsertJson, List<OpportunityLineItem>.Class) : new List<OpportunityLineItem>();
        
        if(OLIsToUpsertList.size() > 0) {
            try {
                upsert(OLIsToUpsertList);
            } catch (Exception ex) {
                //throw new AuraException(ex.getMessage());
                String errorMsg = ex.getMessage();
                if(errorMsg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                    errorMsg = errorMsg.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ', ': ');
                }
                throw new AuraHandledException(errorMsg);
            }
            
        }
    }
    
    /**
    * @description Generic function to return possible options for a picklist field
    * @param describeFieldResult picklist field to get the options from
    * @return possible values in a LocalSelect form
    */
    public static List<LocalSelect> getPicklistLabels(Schema.DescribeFieldResult describeFieldResult){
    	List<LocalSelect> pickListLabels= new List<LocalSelect>();
        
        for(Schema.PicklistEntry pickListVal : describeFieldResult.getPicklistValues()){            
			pickListLabels.add(new LocalSelect(pickListVal.getLabel(), pickListVal.getLabel()));
		}     
        
        return pickListLabels;
    }
    
    /**
	* @description Get possible Status options for OpportunityLineItem
    */
    @AuraEnabled
    public static List<LocalSelect> getOliStatusOptions(){
        return getPicklistLabels(SObjectType.OpportunityLineItem.fields.V2SACL1__Status__c );
    }
    
    /**
	* @description Get possible Sub-Category options for OpportunityLineItem
    */
    @AuraEnabled
    public static List<LocalSelect> getOliSubCategoryOptions(){
        return getPicklistLabels(SObjectType.OpportunityLineItem.fields.V2SACL1__Sub_Category__c);
    }

    @AuraEnabled
    public static User getUserInfo(Id userId){
        return [SELECT Id, LastName, Profile.Name FROM User WHERE Id = : userId LIMIT 1];
    }

    @AuraEnabled
    public static List<V2SACL1__Prod_Sel_Permissions__mdt> getPermissions(){
        string profileName = [SELECT Id, Name FROM Profile WHERE Id =: UserInfo.getProfileId() LIMIT 1].Name;
        List<V2SACL1__Prod_Sel_Permissions__mdt> permissionList = [SELECT V2SACL1__Username__c, V2SACL1__Profile_Name__c, V2SACL1__IsUser__c, V2SACL1__Is_Active__c, V2SACL1__Mark_booked_button_enabled__c, V2SACL1__Allow_Select_Previous_Months__c FROM V2SACL1__Prod_Sel_Permissions__mdt WHERE V2SACL1__Is_Active__c = true AND V2SACL1__Username__c =: UserInfo.getUserName()];

        if (permissionList.size() == 0) {
            permissionList = [SELECT V2SACL1__Username__c, V2SACL1__Profile_Name__c, V2SACL1__IsUser__c, V2SACL1__Is_Active__c, V2SACL1__Mark_booked_button_enabled__c, V2SACL1__Allow_Select_Previous_Months__c FROM V2SACL1__Prod_Sel_Permissions__mdt WHERE V2SACL1__Is_Active__c = true AND V2SACL1__Profile_Name__c =: profileName];
        }
        system.debug('permissions');
        system.debug(permissionList);
        return permissionList;
    }
     
}