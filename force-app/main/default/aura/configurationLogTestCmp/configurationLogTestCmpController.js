({
	afterScriptsLoadedJSPDF : function(component, event, helper) {
        console.log('Loaded JSPDF');
        
        var pdf = new jsPDF('p', 'pt', 'letter');
        console.log('var pdf =', pdf);
	},
    
	afterScriptsLoadedHTML2PDF : function(component, event, helper) {
        console.log('Loaded HTML2PDF');
	},
    
    generateTestPDF : function(component, event, helper) {
        console.log('generateTestPDF');
        var pdf = new jsPDF('p', 'pt', 'letter');
        console.log('var pdf =', pdf);
    
        var canvas = pdf.canvas;
        canvas.height = 72 * 11;
        canvas.width = 72 * 8.5;
        console.log('var canvas =', canvas);
    
    	var html = '<html><head><style>td{ border: 1px solid black; padding: 5px; } th{border: 1px solid black; padding: 5px; color: black; background-color: #0391E4;}</style></head>'
    	+ '<body><table style="width:100%"><tr><th>Firstname</th><th>Lastname</th><th>Age</th></tr><tr><td>Jill</td><td>Smith</td><td>50</td></tr><tr><td>Eve</td><td>Jackson</td><td>94</td></tr></table></body>'
    	+ '</html>';
        window.setTimeout(function() {
            
            html2pdf(html, pdf, function(pdf) {
                console.log('html2pdf');
                var iframe = document.createElement('iframe');
                iframe.setAttribute('style','position:absolute;right:0; top:0; bottom:0; height:100%; width:500px');
                document.body.appendChild(iframe);
                iframe.src = pdf.output('datauristring');
                
                var div = document.createElement('pre');
                div.innerText=pdf.output();
                document.body.appendChild(div);
            });
        }, 5000);
        
	}
})