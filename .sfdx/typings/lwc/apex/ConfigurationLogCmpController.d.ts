declare module "@salesforce/apex/ConfigurationLogCmpController.searchForComponents" {
  export default function searchForComponents(param: {componentList: any, searchCriteria: any, sessionId: any}): Promise<any>;
}
declare module "@salesforce/apex/ConfigurationLogCmpController.getAllComponents" {
  export default function getAllComponents(): Promise<any>;
}
declare module "@salesforce/apex/ConfigurationLogCmpController.saveConfigLogAttchment" {
  export default function saveConfigLogAttchment(param: {jsonConfigLog: any, jsonTable: any}): Promise<any>;
}
declare module "@salesforce/apex/ConfigurationLogCmpController.saveConfigLogPackage" {
  export default function saveConfigLogPackage(param: {jsonConfigLog: any, jsonTable: any}): Promise<any>;
}
declare module "@salesforce/apex/ConfigurationLogCmpController.getOrCreateConfigLogObject" {
  export default function getOrCreateConfigLogObject(param: {jsonConfigLog: any}): Promise<any>;
}
