declare module "@salesforce/apex/reusableMultiSelectLookupController.fetchLookUpValues" {
  export default function fetchLookUpValues(param: {searchKeyWord: any, ObjectName: any, ExcludeitemsList: any}): Promise<any>;
}
