declare module "@salesforce/apex/V2_PipelineMgr_LookupController.search" {
  export default function search(param: {searchTerm: any, selectedIds: any, opportunityId: any, platform: any}): Promise<any>;
}
