declare module "@salesforce/apex/ConfigurationLogAttachmentsCmp.getConfigLogAttachments" {
  export default function getConfigLogAttachments(param: {configurationLogId: any}): Promise<any>;
}
declare module "@salesforce/apex/ConfigurationLogAttachmentsCmp.mergeSelectedAttachments" {
  export default function mergeSelectedAttachments(param: {attachments: any}): Promise<any>;
}
