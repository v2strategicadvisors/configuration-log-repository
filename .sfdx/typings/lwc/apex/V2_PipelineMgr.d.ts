declare module "@salesforce/apex/V2_PipelineMgr.getExistingPipelineDetailsByPlatform" {
  export default function getExistingPipelineDetailsByPlatform(param: {opportunityId: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_PipelineMgr.getMetadata" {
  export default function getMetadata(param: {opportunityId: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_PipelineMgr.savePipelineDetails" {
  export default function savePipelineDetails(param: {pipelineDetailsToUpsertJson: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_PipelineMgr.getPipelineDetailStatusOptions" {
  export default function getPipelineDetailStatusOptions(): Promise<any>;
}
declare module "@salesforce/apex/V2_PipelineMgr.getPipelineDetailSubCategoryOptions" {
  export default function getPipelineDetailSubCategoryOptions(): Promise<any>;
}
declare module "@salesforce/apex/V2_PipelineMgr.getUserInfo" {
  export default function getUserInfo(param: {userId: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_PipelineMgr.getPermissions" {
  export default function getPermissions(): Promise<any>;
}
