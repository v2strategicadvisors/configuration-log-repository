declare module "@salesforce/apex/V2_AccEstimatesManager.getChannelPicklistValues" {
  export default function getChannelPicklistValues(): Promise<any>;
}
declare module "@salesforce/apex/V2_AccEstimatesManager.getFilterOptions" {
  export default function getFilterOptions(): Promise<any>;
}
declare module "@salesforce/apex/V2_AccEstimatesManager.getEstimateSummaryData" {
  export default function getEstimateSummaryData(param: {year: any, quarter: any, accountList: any, accountName: any, offset: any, paginationSize: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_AccEstimatesManager.save" {
  export default function save(param: {quarterForecastJson: any, year: any, channel: any, accountId: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_AccEstimatesManager.getDefaultSettings" {
  export default function getDefaultSettings(): Promise<any>;
}
