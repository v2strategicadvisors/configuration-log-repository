declare module "@salesforce/apex/V2_ProdSel.getExistingOLIsByPlatform" {
  export default function getExistingOLIsByPlatform(param: {opportunityId: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_ProdSel.getMetadata" {
  export default function getMetadata(param: {opportunityId: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_ProdSel.saveOpportunityLineItems" {
  export default function saveOpportunityLineItems(param: {OLIsToUpsertJson: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_ProdSel.getOliStatusOptions" {
  export default function getOliStatusOptions(): Promise<any>;
}
declare module "@salesforce/apex/V2_ProdSel.getOliSubCategoryOptions" {
  export default function getOliSubCategoryOptions(): Promise<any>;
}
declare module "@salesforce/apex/V2_ProdSel.getUserInfo" {
  export default function getUserInfo(param: {userId: any}): Promise<any>;
}
declare module "@salesforce/apex/V2_ProdSel.getPermissions" {
  export default function getPermissions(): Promise<any>;
}
